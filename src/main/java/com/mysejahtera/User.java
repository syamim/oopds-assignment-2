package com.mysejahtera;

import java.util.*;

public class User {
  List<Recipient> recipientsList = new ArrayList<>();
  List<VaccineCenter> vacList = new ArrayList<>();
  List<MinHealth> mohList = new ArrayList<>();
  public User(){}
  public User(List<Recipient> recipientsList, List<VaccineCenter> vacList, List<MinHealth> mohList){
    this.recipientsList = recipientsList;
    this.vacList = vacList;
    this.mohList = mohList;
  }
}
