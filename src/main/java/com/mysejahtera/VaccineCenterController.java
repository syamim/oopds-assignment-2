package com.mysejahtera;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VaccineCenterController {
  User users;

  public VaccineCenterController(){}
  
  public VaccineCenterController(User users){
    this.users = users;
  }

  
  /** 
   * @param date
   * @param dose
   * @param current_user
   * @param recInd
   * @param users
   * @return User
   */
  public User setAppoinment(String date, int dose, VaccineCenter current_user, String recInd, User users){
    int recIn = Integer.parseInt(recInd);
    Recipient rec = users.recipientsList.get(recIn);  
    if(dose == 1){
      rec.first_dose.put("date", date);
      rec.updateStatus("1st Dose Appointment");
    }else{
      rec.first_dose.put("date", date);
      rec.updateStatus("2nd Dose Appointment");
    }
    users.recipientsList.set(recIn, rec);
    return users;
  }

  
  /** 
   * @param users
   * @param center
   * @param dose
   * @return List<Recipient>
   */
  public List<Recipient> getRecipients(User users, String center, int dose){
    List<Recipient> recipients = new ArrayList<>();
    users.recipientsList.forEach(recipient -> {
      if (dose == 1){
        if(recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(center)){
            recipients.add(recipient);
          }
        }
      }else{
        if(recipient.second_dose.get("isEmpty").equals("false")){
          if (recipient.second_dose.get("center").equals(center)){
            recipients.add(recipient);
          }
        }
      }
    });
    return recipients;
  }
}
