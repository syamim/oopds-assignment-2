package com.mysejahtera;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.*;

import java.util.*;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class VacView {
  MainController controller;
  VaccineCenter current_user;
  VaccineCenterController vacController;
  int current_userInd;
  public VacView(MainController controller, int index){
    this.controller = controller;
    this.current_userInd = index;
    this.vacController = new VaccineCenterController();
  }
  
  public void mainMenu(){
    User users = this.controller.getUsers();
    current_user = users.vacList.get(current_userInd);
    GridPane view = new GridPane();
    Label title = new Label("Hello, " + current_user.name);
    title.setFont(new Font("Arial", 16));
    GridPane tG = new GridPane();
    tG.addRow(0, title);
    tG.setAlignment(Pos.CENTER);
    view.addRow(0, tG);
    Button viewAll = new Button("View All Recipient");
    Button appointment = new Button("Set Appointment");
    Button simulator = new Button("Hall Simulator");
    Button statistic = new Button("View Statistic");
    Button logout = new Button("Logout");
    GridPane menu = new GridPane();
    view.addRow(1, menu);
    menu.addColumn(0, viewAll);
    menu.addColumn(0, appointment);
    menu.addColumn(0, simulator);
    menu.addColumn(0, statistic);
    menu.addColumn(0, logout);

    viewAll.setMinWidth(150);
    appointment.setMinWidth(150);
    simulator.setMinWidth(150);
    statistic.setMinWidth(150);
    logout.setMinWidth(150);
    menu.setAlignment(Pos.CENTER);
    menu.setVgap(3);
    view.setVgap(3);
    view.setAlignment(Pos.CENTER);

    Scene scene = new Scene(view, 300, 300);
    Stage stage = new Stage();
    stage.setTitle("Vaccince Center Menu");
    stage.setScene(scene);
    stage.show();

    // event listener for all button
    viewAll.setOnAction(e->{
      viewAllRec();  
    });
    appointment.setOnAction(e->{
      setAppointment();
    });
    statistic.setOnAction(e->{
      statistic();
    });
    simulator.setOnAction(e->{
      hallSimulator();
    });
    logout.setOnAction(e->{
      stage.close();
    });
  }

  public void statistic(){
    User users = this.controller.getUsers();
    List<Recipient> firstRecipients = this.vacController.getRecipients(users, current_user.name, 1);
    List<Recipient> secondRecipients = this.vacController.getRecipients(users, current_user.name, 2);
    Button totalVac = new Button("Total Vaccination");
    Button byDay = new Button("Total By Day");
    Button totalLeft = new Button("Total Vaccine Available");
    GridPane buttons = new GridPane();
    GridPane buttons1 = new GridPane();
    GridPane buttons2 = new GridPane();
    buttons1.addRow(0, totalVac, byDay);
    buttons2.addRow(0, totalLeft);
    buttons.addRow(0, buttons1);
    buttons.addRow(1, buttons2);
    buttons.setAlignment(Pos.CENTER);
    buttons1.setAlignment(Pos.CENTER);
    buttons2.setAlignment(Pos.CENTER);
    buttons.setVgap(4);
    buttons1.setHgap(4);
    GridPane view = new GridPane();
    view.setAlignment(Pos.CENTER); 
    view.addRow(0, buttons);
    // generate table for total vaccination
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    LocalDateTime now = LocalDateTime.now();
    var totalVacData = new Object(){
      int pending, first_complete, first_appointment, second_complete, second_appointment;
    };
    var byDayData = new Object(){
      int first_complete, second_complete;
    };
    GridPane totalVacTable = new GridPane();
    for (Recipient recipient: firstRecipients){
      if (recipient.status.equals("Pending")){
        totalVacData.pending++;
      }else if (recipient.status.equals("1st Dose Completed")){
        totalVacData.first_complete++;
        if (recipient.getDose("1", "date_complete").equals(dtf.format(now))){
          byDayData.first_complete++;    
        }
      }else if (recipient.status.equals("1st Dose Appointment")){
        totalVacData.first_appointment++;
      }
    }
    
    for (Recipient recipient: secondRecipients){
      if (recipient.status.equals("2nd Dose Completed")){
        totalVacData.second_complete++;
        if (recipient.getDose("2", "date_complete").equals(dtf.format(now))){
          byDayData.second_complete++;    
        }
      }else if (recipient.status.equals("2nd Dose Appointment")){
        totalVacData.second_appointment++;
      }
    }
    
    
    totalVacTable.addRow(0, new Label("Pending"), new Label(Integer.toString(totalVacData.pending)));
    totalVacTable.addRow(1, new Label("1st Dose Appointment"), new Label(Integer.toString(totalVacData.first_appointment)));
    totalVacTable.addRow(2, new Label("1st Dose Completed"), new Label(Integer.toString(totalVacData.first_complete)));
    totalVacTable.addRow(3, new Label("2nd Dose Appointment"), new Label(Integer.toString(totalVacData.second_appointment)));
    totalVacTable.addRow(4, new Label("2nd Dose Completed"), new Label(Integer.toString(totalVacData.second_complete)));
    view.addRow(1, totalVacTable);
    totalVacTable.setAlignment(Pos.CENTER);
    totalVacTable.setVgap(3);
    totalVacTable.setHgap(5);
    // end generate table for total vaccination
    
    // generate by day table
    var helper = new Object(){
      GridPane byDayTable = new GridPane();
    };
    // GridPane byDayTable = new GridPane();
    helper.byDayTable.setAlignment(Pos.CENTER);
    helper.byDayTable.setHgap(4);
    helper.byDayTable.setVgap(4);
    helper.byDayTable.addRow(0, new Label("1st Dose Complete"), new Label(Integer.toString(byDayData.first_complete)));
    helper.byDayTable.addRow(1, new Label("2nd Dose Complete"), new Label(Integer.toString(byDayData.second_complete)));
    TextField dateInp = new TextField();
    Button dateBtn = new Button("View Data");
    byDay.setOnAction(e->{
      // GridPane byDayGp = new GridPane();
      view.getChildren().removeIf( node -> GridPane.getRowIndex(node) == 1);
      view.addRow(1, helper.byDayTable);
      GridPane buttonGp = new GridPane();
      buttonGp.addRow(0, new Label("Enter Date : "), dateInp, dateBtn);
      buttonGp.setAlignment(Pos.CENTER);
      buttonGp.setHgap(3);
      view.addRow(2, buttonGp);
      // view.addRow(1, helper.byDayGp);
    });
    totalVac.setOnAction(e->{
      view.getChildren().removeIf( node -> GridPane.getRowIndex(node) == 1);
      view.getChildren().removeIf( node -> GridPane.getRowIndex(node) == 2);
      view.addRow(1, totalVacTable);
    });
    dateBtn.setOnAction(e->{
      byDayData.first_complete = 0;
      byDayData.second_complete = 0;
      for (Recipient recipient: firstRecipients){
        if (recipient.status.equals("1st Dose Completed")){
          if (recipient.getDose("1", "date_complete").equals(dateInp.getText())){
            byDayData.first_complete++;    
          }
        }
      }
      
      for (Recipient recipient: secondRecipients){
        if (recipient.status.equals("2nd Dose Completed")){
          if (recipient.getDose("2", "date_complete").equals(dateInp.getText())){
            byDayData.second_complete++;    
          }
        }
      }
      
      view.getChildren().remove(helper.byDayTable);
      helper.byDayTable = new GridPane();
      helper.byDayTable.addRow(0, new Label("1st Dose Complete"), new Label(Integer.toString(byDayData.first_complete)));
      helper.byDayTable.addRow(1, new Label("2nd Dose Complete"), new Label(Integer.toString(byDayData.second_complete)));
      view.addRow(1, helper.byDayTable);
      // view.add
    });

    Scene scene = new Scene(view, 500, 500);
    Stage stage = new Stage();
    stage.setTitle("Statistic");
    stage.setScene(scene);
    stage.show();

  }

  public void hallSimulator(){
    Button next = new Button("Next");
    var helper = new Object(){
      List<Recipient> appointmentRec = new ArrayList<>();
      List<Recipient> vaccinatedRec = new ArrayList<>();
      List<Recipient> seniorRec = new ArrayList<>();
      List<Recipient> normalRec = new ArrayList<>();
      List<String> vaccineStack = new ArrayList<>();
      int count = 0;
      User users;
      GridPane view = new GridPane();
    };
    helper.users = this.controller.getUsers();
    // can optimize using getRecipients in VacController
    helper.users.recipientsList.forEach(recipient->{
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
      LocalDateTime now = LocalDateTime.now();
      Random rand = new Random();
      if(recipient.first_dose.get("isEmpty").equals("false")){
        if (recipient.first_dose.get("center").equals(current_user.name)){
          if (recipient.first_dose.get("date").equals(dtf.format(now))){
            helper.appointmentRec.add(recipient);
            int age = Integer.parseInt(recipient.age);
            if(recipient.status.equals("1st Dose Completed")){
              helper.vaccinatedRec.add(recipient);
            }else if (age < 60){
              helper.vaccineStack.add(Integer.toString(rand.nextInt(1000)));
              helper.normalRec.add(recipient);
            }else if(age > 59){
              helper.vaccineStack.add(Integer.toString(rand.nextInt(1000)));
              helper.seniorRec.add(recipient);
            }
          }
        }
      }
      if(recipient.second_dose.get("isEmpty").equals("false")){
        if (recipient.second_dose.get("center").equals(current_user.name)){
          if (recipient.second_dose.get("date").equals(dtf.format(now))){
            helper.appointmentRec.add(recipient);
            int age = Integer.parseInt(recipient.age);
            if(recipient.status.equals("2nd Dose Completed")){
              helper.vaccinatedRec.add(recipient);
            }else if (age < 60){
              helper.vaccineStack.add(Integer.toString(rand.nextInt(1000)));
              helper.normalRec.add(recipient);
            }else if(age > 59){
              helper.vaccineStack.add(Integer.toString(rand.nextInt(1000)));
              helper.seniorRec.add(recipient);
            }
          }
        }
      }
    });
    
    Label normalTitle = new Label("Normal Queue");
    normalTitle.setFont(new Font("Arial", 16));
    Label stackTitle = new Label("Stack of Vaccine");
    stackTitle.setFont(new Font("Arial", 16));
    Label vaccinatedTitle = new Label("Vaccinated");
    vaccinatedTitle.setFont(new Font("Arial", 16));
    Label seniorTitle = new Label("Senior Queue");
    seniorTitle.setFont(new Font("Arial", 16));
    
    // table view 
    TableView nameAgeTable = new TableView();
    TableColumn<Recipient, String> nameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> ageCol = new TableColumn<>("Age");
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    ageCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().age));
    nameAgeTable.getColumns().addAll(nameCol,ageCol);
    nameAgeTable.setMaxHeight(150);
    nameAgeTable.setMinWidth(150);
    nameCol.setMinWidth(130);
    ageCol.setMinWidth(20);
    ageCol.setStyle("-fx-alignment: center");
    
    TableView nameAgeTable2 = new TableView();
    TableColumn<Recipient, String> nameCol2 = new TableColumn<>("Name");
    TableColumn<Recipient, String> ageCol2 = new TableColumn<>("Age");
    nameCol2.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    ageCol2.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().age));
    nameAgeTable2.getColumns().addAll(nameCol2,ageCol2);
    nameAgeTable2.setMaxHeight(150);
    nameAgeTable2.setMinWidth(150);
    nameCol2.setMinWidth(130);
    ageCol2.setMinWidth(20);
    ageCol2.setStyle("-fx-alignment: center");
    
    TableView vacStackTable = new TableView();
    TableColumn<String, String> batchCol = new TableColumn<>("Name");
    batchCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue()));
    vacStackTable.getColumns().addAll(batchCol);
    vacStackTable.setMaxHeight(150);
    vacStackTable.setMaxWidth(80);
    batchCol.setMinWidth(70);
    batchCol.setMaxWidth(70);
    batchCol.setStyle("-fx-alignment: center");
    
    TableView vaccinatedTable = new TableView();
    TableColumn<Recipient, String> vbatchCol = new TableColumn<>("Batch No");
    TableColumn<Recipient, String> vnameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> vageCol = new TableColumn<>("Age");
    vbatchCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("1", "batch")));
    vnameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    vageCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().age));
    vaccinatedTable.getColumns().addAll(vbatchCol, vnameCol, vageCol);
    vaccinatedTable.setMaxHeight(150);
    vaccinatedTable.setMaxWidth(250);
    vbatchCol.setMinWidth(50);
    vbatchCol.setMaxWidth(50);
    vbatchCol.setStyle("-fx-alignment: center");
    vnameCol.setMinWidth(130);
    vageCol.setMinWidth(20);
    vageCol.setStyle("-fx-alignment: center");
    
    helper.view.setVgap(4);
    helper.view.setHgap(5);
    helper.view.setAlignment(Pos.CENTER);
    // end table view
    GridPane seniorList = new GridPane();
    seniorList.setAlignment(Pos.CENTER);
    helper.seniorRec.forEach(e->{
      nameAgeTable.getItems().add(e);
    });
    seniorList.addRow(0, nameAgeTable);
    
    nameAgeTable2.getItems().clear();
    GridPane normalList = new GridPane();
    helper.normalRec.forEach(e->{
      nameAgeTable2.getItems().add(e);
    });
    normalList.addRow(0, nameAgeTable2);
    helper.view.addRow(0, seniorTitle, normalTitle);
    helper.view.addRow(1, seniorList, normalList);
    
    GridPane stackList = new GridPane();
    helper.vaccineStack.forEach(e->{
      vacStackTable.getItems().add(e);
    });
    stackList.addRow(0, vacStackTable);
    
    GridPane vaccinatedList = new GridPane();
    helper.vaccinatedRec.forEach(e->{
      vaccinatedTable.getItems().add(e);
    });
    vaccinatedList.addRow(0, vaccinatedTable);
    helper.view.addRow(2, stackTitle, vaccinatedTitle);
    helper.view.addRow(3, stackList, vaccinatedList);

    GridPane mainPane = new GridPane();
    Label title = new Label("Hall Simulator");
    title.setFont(new Font("Arial", 18));
    title.setStyle("-fx-font-weight: bold;");
    GridPane titleGp = new GridPane();
    titleGp.setAlignment(Pos.CENTER);
    titleGp.addRow(0, title);
    mainPane.addRow(0, titleGp);
    mainPane.addRow(1, helper.view);
    GridPane buttonGp = new GridPane();
    buttonGp.setAlignment(Pos.CENTER);
    buttonGp.addRow(0, next);
    next.setMinWidth(150);
    mainPane.addRow(2, buttonGp);
    mainPane.setAlignment(Pos.CENTER);
    mainPane.setVgap(4);

    Scene scene = new Scene(mainPane, 600, 600);
    Stage stage = new Stage();
    stage.setTitle("Hall Simulator");
    stage.setScene(scene);
    stage.show();

    next.setOnAction(e->{
      var temp = new Object(){
        int count = 0;
        int selectedInd;
        String vaccineBatch;
      };
      if (helper.appointmentRec.size() == 0){
        stage.close();
      }
      if (helper.appointmentRec.size() != 0){
        helper.appointmentRec.remove(helper.appointmentRec.size()-1);
        temp.vaccineBatch = helper.vaccineStack.get(helper.vaccineStack.size()-1);
        helper.vaccineStack.remove(helper.vaccineStack.size()-1);
        if (helper.seniorRec.size() != 0){
          helper.vaccinatedRec.add(helper.seniorRec.get(helper.seniorRec.size()-1));
          helper.seniorRec.remove(helper.seniorRec.size() - 1);
        }else{
          helper.vaccinatedRec.add(helper.normalRec.get(helper.normalRec.size()-1));
          helper.normalRec.remove(helper.normalRec.size()-1);
        }
        Recipient selectedUser = helper.vaccinatedRec.get(helper.vaccinatedRec.size()-1);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        // check if second dose or first dose need to be set
        if (selectedUser.first_dose.get("batch").equals("Not Assign")){
          selectedUser.first_dose.put("batch", temp.vaccineBatch);
          selectedUser.first_dose.put("date_complete", dtf.format(now));
          selectedUser.updateStatus("1st Dose Completed");
        }else{ // set second dose
          selectedUser.second_dose.put("batch", temp.vaccineBatch);
          selectedUser.second_dose.put("date_complete", dtf.format(now));
          selectedUser.updateStatus("2nd Dose Completed");
        }
        // loop to find index in main recipient list to be replace
        helper.users.recipientsList.forEach(recipient->{
          if(recipient.name.equals(selectedUser.name)){
            temp.selectedInd = temp.count;
          }
          temp.count++; 
        });
        // update recipient data
        helper.users.recipientsList.set(temp.selectedInd, selectedUser);
        helper.users = this.controller.testupdateUsers(helper.users);
        // end update recipient data
        
        // recreate view and remove the group and replace with temp group
        nameAgeTable.getItems().clear();
        nameAgeTable2.getItems().clear();
        vacStackTable.getItems().clear();
        vaccinatedTable.getItems().clear();
        for (int i = 0; i < helper.seniorRec.size(); i++) {
          nameAgeTable.getItems().add(helper.seniorRec.get(i));
        }
        
        for (int i = 0; i < helper.normalRec.size(); i++) {
          nameAgeTable2.getItems().add(helper.normalRec.get(i));
        }
        
        for (int i = 0; i < helper.vaccineStack.size(); i++) {
          vacStackTable.getItems().add(helper.vaccineStack.get(i));
        }
        
        for (int i = 0; i < helper.vaccinatedRec.size(); i++) {
          vaccinatedTable.getItems().add(helper.vaccinatedRec.get(i));
        };
        // end recreate view and remove the group and replacewith temp group
      }
    });
  }

  public void setAppointment(){
    // idea - add select button to list and action will add the index to arraylist to assign
    GridPane view = new GridPane();
    Button firstDoseBtn = new Button("First Dose");
    Button secondDoseBtn = new Button("Second Dose");
    GridPane btnGp = new GridPane();
    btnGp.addRow(0, firstDoseBtn, secondDoseBtn);
    btnGp.setHgap(5);
    btnGp.setAlignment(Pos.CENTER);
    view.setAlignment(Pos.CENTER);
    view.addRow(0, btnGp);
    var helper = new Object(){
      User users;
      int dose = 1;
      int count = 1;
    };
    helper.users = this.controller.getUsers();
    GridPane firstView = new GridPane();
    GridPane secondView = new GridPane();
    view.addRow(2, firstView);
    
    TableView firstTable = new TableView();
    TableColumn<Recipient, String> noCol = new TableColumn<>("NO");
    TableColumn<Recipient, String> nameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> limitCol = new TableColumn<>("Status");
    noCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    limitCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().status));
    firstTable.getColumns().addAll(noCol,nameCol,limitCol);
    
    firstView.addRow(0,firstTable);
    current_user = helper.users.vacList.get(current_userInd);
    helper.users.recipientsList.forEach(e->{
      if (e.first_dose.get("isEmpty").equals("false")){
        if (e.first_dose.get("center").equals(current_user.name)){
          if (e.first_dose.get("date").equals("Not Assign")){
            firstTable.getItems().add(e);
          }
        }
      }
    });

    firstTable.setMaxHeight(300);
    firstTable.setMinWidth(400);
    noCol.setMinWidth(20);
    nameCol.setMinWidth(180);
    limitCol.setMinWidth(100);
    noCol.setStyle("-fx-alignment: center");
    limitCol.setStyle("-fx-alignment: center");

    firstDoseBtn.setOnAction(e->{
      view.getChildren().remove(secondView);
      helper.users = this.controller.getUsers();
      view.addRow(2, firstView);
      helper.dose = 1;
    });
    
    TableView secondTable = new TableView();
    TableColumn<Recipient, String> snoCol = new TableColumn<>("NO");
    TableColumn<Recipient, String> snameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> slimitCol = new TableColumn<>("Status");
    snoCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    snameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    slimitCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().status));
    secondTable.getColumns().addAll(snoCol,snameCol,slimitCol);
    secondView.addRow(0,secondTable);

    current_user = helper.users.vacList.get(current_userInd);
    helper.users.recipientsList.forEach(e->{
      if (e.second_dose.get("isEmpty").equals("false")){
        if (e.second_dose.get("center").equals(current_user.name)){
          if (e.second_dose.get("date").equals("Not Assign")){
            secondTable.getItems().add(e);
          }
        }
      }
    });

    secondTable.setMaxHeight(300);
    secondTable.setMinWidth(400);
    snoCol.setMinWidth(20);
    snameCol.setMinWidth(180);
    slimitCol.setMinWidth(100);
    snoCol.setStyle("-fx-alignment: center");
    slimitCol.setStyle("-fx-alignment: center");

    secondDoseBtn.setOnAction(e->{
      view.getChildren().remove(firstView);
      helper.users = this.controller.getUsers();
      view.addRow(2, secondView);
      helper.dose = 2;
    });

    GridPane form = new GridPane();
    TextField recInd = new TextField();
    TextField dateInp = new TextField();
    Button submit = new Button("Assign");
    form.addRow(0, new Label("Recipient No : "), recInd);
    form.addRow(1, new Label("Appointment Date : "), dateInp);
    form.addColumn(1, submit);
    submit.setMinWidth(150);
    form.setAlignment(Pos.CENTER);
    form.setVgap(3);
    view.setVgap(3);
    view.addRow(3, form);
    submit.setOnAction(e->{
      User userUpdated = vacController.setAppoinment(dateInp.getText(), helper.dose, current_user, recInd.getText(), helper.users);
      this.controller.updateUsers(userUpdated);
      view.addRow(6, new Label("Appointment have been set. Please reopen window to update"));
    });
    Scene scene = new Scene(view, 500, 500);
    Stage stage = new Stage();
    stage.setTitle("Set Appointment");
    stage.setScene(scene);
    stage.show();
  }


  public void viewAllRec(){
    GridPane view = new GridPane();
    Button firstDoseBtn = new Button("First Dose");
    Button secondDoseBtn = new Button("Second Dose");
    GridPane btnGp = new GridPane();
    btnGp.addRow(0, firstDoseBtn, secondDoseBtn);
    btnGp.setAlignment(Pos.CENTER);
    btnGp.setHgap(5);
    view.addRow(0, btnGp);
    view.setVgap(3);
    Label title = new Label("Recipient List");
    title.setStyle("-fx-font-weight: bold;");
    title.setFont(new Font("Arial", 16));
    view.setAlignment(Pos.CENTER);
    view.addRow(1, title);
    Group firstDoseGp = new Group();
    Group secondDoseGp = new Group();
    var helper = new Object(){
      User users;
    };
    helper.users = this.controller.getUsers();
    view.addRow(2, firstDoseGp);

    TableView firstTable = new TableView();
    TableColumn<Recipient, String> noCol = new TableColumn<>("NO");
    TableColumn<Recipient, String> nameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> totalCol = new TableColumn<>("1st Dose Date");
    noCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    totalCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("1", "date")));
    firstTable.getColumns().addAll(noCol,nameCol,totalCol);

    current_user = helper.users.vacList.get(current_userInd);
    helper.users.recipientsList.forEach(e->{
      if (e.first_dose.get("isEmpty").equals("false")){
        if (e.first_dose.get("center").equals(current_user.name)){
          firstTable.getItems().add(e);
        }
      }
    });
    firstDoseGp.getChildren().add(firstTable);

    firstTable.setMaxHeight(400);
    firstTable.setMinWidth(400);
    noCol.setStyle("-fx-alignment: center");
    totalCol.setStyle("-fx-alignment: center");
    noCol.setMinWidth(20);
    nameCol.setMinWidth(180);
    totalCol.setMinWidth(150);

    firstDoseBtn.setOnAction(e->{
      view.getChildren().remove(secondDoseGp);
      helper.users = this.controller.getUsers();
      view.addRow(2, firstDoseGp);
    });
    
    TableView secondTable = new TableView();
    TableColumn<Recipient, String> snoCol = new TableColumn<>("NO");
    TableColumn<Recipient, String> snameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> stotalCol = new TableColumn<>("2nd Dose Date");
    snoCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    snameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    stotalCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("2", "date")));
    secondTable.getColumns().addAll(snoCol,snameCol,stotalCol);
    
    current_user = helper.users.vacList.get(current_userInd);
    helper.users.recipientsList.forEach(e->{
      if (e.second_dose.get("isEmpty").equals("false")){
        if (e.second_dose.get("center").equals(current_user.name)){
          secondTable.getItems().add(e);
        }
      }
    });

    secondTable.setMaxHeight(400);
    secondTable.setMinWidth(400);
    snoCol.setStyle("-fx-alignment: center");
    stotalCol.setStyle("-fx-alignment: center");
    snoCol.setMinWidth(20);
    snameCol.setMinWidth(180);
    stotalCol.setMinWidth(150);
    
    secondDoseGp.getChildren().add(secondTable);
    secondDoseBtn.setOnAction(e->{
      view.getChildren().remove(firstDoseGp);
      helper.users = this.controller.getUsers();
      view.addRow(2, secondDoseGp);
    });

    Scene scene = new Scene(view, 500, 500);
    Stage stage = new Stage();
    stage.setTitle("List Recipient");
    stage.setScene(scene);
    stage.show();
  }
}

