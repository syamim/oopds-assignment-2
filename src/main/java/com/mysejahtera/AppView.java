package com.mysejahtera;

import java.io.IOException;
import java.rmi.server.ObjID;
import java.util.HashMap;
import java.util.Map;

import com.opencsv.exceptions.CsvException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.*;
import javafx.stage.*;

public class AppView {
  GridPane view;
  User users;
  MainController controller;
  
  public AppView(User users, MainController controller){
    this.users = users;
    this.controller = controller;
    // create main view for login and redirect from the function
    configureWindow();
    mainMenu();
  }

  
  /** 
   * @return Parent
   */
  public Parent asParent (){
    return view;
  }

  // Build view
  public void configureWindow(){
    view = new GridPane();

    ColumnConstraints leftCol = new ColumnConstraints();
    leftCol.setHalignment(HPos.RIGHT);
    leftCol.setHgrow(Priority.NEVER);

    ColumnConstraints rightCol = new ColumnConstraints();
    rightCol.setHgrow(Priority.SOMETIMES);

    view.getColumnConstraints().addAll(leftCol, rightCol);

    view.setAlignment(Pos.CENTER);
    view.setHgap(5);
    view.setVgap(10);
  }

  private void loginLayout() {
    GridPane loginView = new GridPane();
    TextField username = new TextField();
    TextField password = new TextField();
    Button submit = new Button("Login");
    var helper = new Object(){
      Map<String, String> response = new HashMap<>();
    };
    
    loginView.addRow(0, new Label("Username "), username);
    loginView.addRow(1, new Label("Password "), password);
    GridPane gp = new GridPane();
    gp.addColumn(0, submit);
    GridPane main = new GridPane();
    main.addRow(0, loginView);
    main.addRow(1, gp);
    
    loginView.setVgap(3);
    main.setAlignment(Pos.CENTER);
    main.setVgap(3);
    gp.setAlignment(Pos.CENTER);
    submit.setMinWidth(200);

    Scene scene = new Scene(main, 500, 150);
    Stage stage = new Stage();
    stage.setTitle("Login to Your Account");
    stage.setScene(scene);
    stage.show();
    submit.setOnAction((e)->{
      helper.response = this.controller.login(username.getText(), password.getText());
      if (helper.response.get("authenticated").equals("true")){
        stage.close();
        // this.controller.redirectController(helper.response.get("type"), helper.response.get("userIndex"));
        if (helper.response.get("type").equals("recipient")){
          RecipientView recV = new RecipientView(controller, Integer.parseInt(helper.response.get("userIndex")));
          recV.mainMenu();
        }else if (helper.response.get("type").equals("vac")){
          VacView vacV = new VacView(controller, Integer.parseInt(helper.response.get("userIndex")));
          vacV.mainMenu();
        }else if (helper.response.get("type").equals("moh")){
          MohView mohV = new MohView(controller, Integer.parseInt(helper.response.get("userIndex")));
          mohV.mainMenu();
        }
      }else{
        System.out.println("Display Error");
      }
    });
  }


  private void registerUser(){
    User users = this.controller.getUsers();
    Recipient lastRec = users.recipientsList.get(users.recipientsList.size() -1);
    int lastId = lastRec.id;

    GridPane view = new GridPane();
    TextField name = new TextField();
    TextField age = new TextField();
    TextField phone_number = new TextField();
    TextField username = new TextField();
    TextField password = new TextField();
    Button submit = new Button("Register Now");
    var helper = new Object(){
      Boolean registered = false;
    };
    Label title = new Label("      Register for Vaccination!");
    GridPane inputGp = new GridPane();
    inputGp.addRow(0, title);
    inputGp.addRow(1, new Label("Name "), name);
    inputGp.addRow(2, new Label("Phone Number "), phone_number);
    inputGp.addRow(3, new Label("Age "), age);
    inputGp.addRow(4, new Label("Username "), username);
    inputGp.addRow(5, new Label("Password "), password);
    GridPane subGp = new GridPane();
    subGp.addRow(0, submit);
    
    view.addRow(0, title);
    view.addRow(1, inputGp);
    view.addRow(2, subGp);
    
    inputGp.setAlignment(Pos.CENTER);
    inputGp.setVgap(3);
    title.setAlignment(Pos.CENTER);
    title.setFont(new Font("Arial", 16));
    title.setStyle("-fx-font-weight: bold;");
    view.setAlignment(Pos.CENTER);
    view.setVgap(4);
    subGp.setAlignment(Pos.CENTER);
    submit.setMinWidth(200);
    Scene scene = new Scene(view, 300, 300);
    Stage stage = new Stage();
    stage.setTitle("Register User");
    stage.setScene(scene);
    submit.setOnAction((e)->{
      Map<String, String> userData = new HashMap<>();
      userData.put("name", name.getText());
      userData.put("age", age.getText());
      userData.put("phone_number", phone_number.getText());
      userData.put("username", username.getText());
      userData.put("password", password.getText());
      helper.registered = this.controller.registerUser(userData, lastId + 1);
      if (helper.registered){
        view.addRow(6, new Label("Registered Successfully. :)"));
      }
    });
    stage.show();
  }

  
  /** 
   * @throws IOException
   * @throws CsvException
   */
  private void exitSystem() throws IOException, CsvException{
    this.controller.exitSystem();
    System.exit(0);
  }

  private void mainMenu(){
    view = new GridPane();
    Button login = new Button("Login");
    Button register = new Button("Register");
    Button exit = new Button("Exit System");
    login.setMinWidth(100);
    register.setMinWidth(100);
    exit.setMinWidth(100);
    view.setAlignment(Pos.CENTER);
    view.setVgap(3);
    view.addColumn(0, login);
    view.addColumn(0, register);
    view.addColumn(0, exit);
    
    login.setOnAction(e -> {
      loginLayout();
    });
    register.setOnAction(e->{
      registerUser();
    });
    exit.setOnAction(e->{
      try {
        exitSystem();
      } catch (IOException e1) {
        e1.printStackTrace();
      } catch (CsvException e1) {
        e1.printStackTrace();
      }
    });
  }
}
