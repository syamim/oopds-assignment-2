package com.mysejahtera;
import java.io.*; 
import java.util.*;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

public class MainInit {
  static String path = "";
  
  public MainInit() throws IOException, CsvException{
    
  }
  
  
  /** 
   * @return User
   * @throws IOException
   * @throws CsvException
   */
  public User readCsv() throws IOException, CsvException{
    List<Recipient> recipientsList = new ArrayList<>();
    List<VaccineCenter> vacList = new ArrayList<>();
    List<MinHealth> mohList = new ArrayList<>();
    String fileName = MainInit.path + "users.csv";
    var helper = new Object(){
      int recID = 0;
      int vacID = 0;
    };
    try (CSVReader reader = new CSVReaderBuilder(new FileReader(fileName))
      .withSkipLines(1) //skip header
      .build()
    ){
      List<String[]> r = reader.readAll();
      r.forEach(userData -> {
        if (userData[7].equals("recipient")){
          Recipient tempRec = new Recipient(userData, helper.recID);
          recipientsList.add(tempRec);
          helper.recID++;
        }else if (userData[7].equals("vac")){
          VaccineCenter tempVac = new VaccineCenter(userData, helper.vacID);
          vacList.add(tempVac);
          helper.vacID++;
        }else{ // moh catcher
          MinHealth tempMoh = new MinHealth(userData);
          mohList.add(tempMoh);
        }
      });
    }

    User users = new User(recipientsList, vacList, mohList);
    // for javafx
    return users;
    // for console
    // mainMenu(users);
  }

  
  /** 
   * @param users
   */
  public void loginLoop(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    var wrapper = new Object() {
      Boolean IsVerified = false;
      Boolean userFound = false;
      Boolean errorExist = false;
    };
    while(!wrapper.IsVerified){
      display.clearConsole();
      display.println("Please login to continue", "success");
      display.print("Enter email : ", "input");
      String email = sc.nextLine();
      display.print("Enter password : ", "input");
      String password = sc.nextLine();
      users.recipientsList.forEach(user ->{
        Map<String, String> temp = user.getCredential();
        if (temp.get("email").equals(email)){
          wrapper.userFound = true;
          if (temp.get("password").equals(password)){
            wrapper.IsVerified = true;
            displayRecipientMenu(user, users);
          }else{
            wrapper.errorExist = true;
          }
        }else{
          wrapper.errorExist = true;
        }
      });

      if (!wrapper.userFound){
        users.vacList.forEach(user ->{
          Map<String, String> temp = user.getCredential();
          if (temp.get("email").equals(email)){
            wrapper.userFound = true;
            if (temp.get("password").equals(password)){
              wrapper.IsVerified = true;
              displayVacMenu(user, users);
            }else{
              wrapper.errorExist = true;
            }
          }else{
            wrapper.errorExist = true;
          }
          return;
        });
      }
      
      if (!wrapper.userFound){
        users.mohList.forEach(user ->{
          Map<String, String> temp = user.getCredential();
          if (temp.get("email").equals(email)){
            wrapper.userFound = true;
            if (temp.get("password").equals(password)){
              wrapper.IsVerified = true;
              displayMohMenu(user, users);
            }else{
              wrapper.errorExist = true;
            }
          }else{
            wrapper.errorExist = true;
          }
          return;
        });
      }
      
      if (wrapper.errorExist && wrapper.IsVerified == false){
        display.clearConsole();
        display.println("Email and password entered is incorrect", "error");
      }
    }
  }
  
  
  /** 
   * @param user
   * @param users
   */
  public void displayRecipientMenu(Recipient user, User users){
    TerminalColor display = new TerminalColor();
    var wrapper = new Object(){
      Boolean endProgram = false;
    };
    while(!wrapper.endProgram){
      display.println("Welcome " + user.name, "success");
      String first_appointment, first_type;
      if (user.first_dose.get("isEmpty").equals("false")){
        first_appointment = user.first_dose.get("date") + " at " + user.first_dose.get("center");
        first_type = user.first_dose.get("type");
      }else{
        first_appointment = "Date not yet assigned";
        first_type = "Not yet assigned";
      }

      String second_appointment, second_type;
      if (user.second_dose.get("isEmpty").equals("false")){
        second_appointment = user.second_dose.get("date") + " at " + user.second_dose.get("center");
        second_type = user.second_dose.get("type");
      }else{
        second_appointment = "Date not yet assigned";
        second_type = "Not yet assigned";
      }
      display.println("Status : " + user.status, "basic");
      if (user.status.equals("Pending")){
        display.println("First Dose", "success");
        display.println("Appointment : " + first_appointment, "warning");
        // display.println("Batch : " + first_type, "warning");
        // display.println("Second Dose", "success");
        // display.println("Appointment : " + second_appointment, "warning");
        // display.println("Batch : " + second_type, "warning");
      }else if(user.status.equals("1st Dose Completed")){
        display.println("First Dose", "success");
        display.print("Complete on : ", "warning");
        display.println(user.first_dose.get("date_complete"), "basic");
        display.print("Batch : ", "warning");
        display.println(user.first_dose.get("batch"), "basic");
        display.print("2nd Dose Appointment : ", "warning");
        if (user.second_dose.get("isEmpty").equals("false")){
          display.println(user.second_dose.get("date"), "basic");
        }else{
          display.println("Center is not assigned yet for second dose", "error");
        }
      }else if(user.status.equals("2nd Dose Completed")){
        display.println("Second Dose", "success");
        display.print("Complete on : ", "warning");
        display.println(user.second_dose.get("date_complete"), "basic");
        display.print("Batch : ", "warning");
        display.println(user.second_dose.get("batch"), "basic");
      }
      Scanner sc = new Scanner(System.in);
      display.print("Press Enter to Logout....", "success");
      sc.nextLine();
      wrapper.endProgram = true;
    }
    display.clearConsole();
    mainMenu(users);
  }

  
  /** 
   * @param user
   * @param users
   */
  public void displayMohMenu(MinHealth user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    MinHealth moh = new MinHealth();
    var wrapper = new Object(){
      Boolean loggedIn = true;
    };
    display.clearConsole();
    while (wrapper.loggedIn) {
      moh.analyticsVaccineByDay(users);
      display.println("Welcome " + user.name, "success");
      display.println("Below is what you can do : ", "input");
      display.println("1. View all recipient", "basic");
      display.println("2. Increase distribution of vaccine to Vaccine Center", "basic");
      display.println("3. Assign recipient to Vaccine Center", "basic");
      display.println("4. View statistic of all Vaccine Center", "basic");
      display.println("5. Logout", "basic");
      display.print("Enter you option : ", "input");
      try {
        int menu = Integer.parseInt(sc.nextLine()); 
        switch (menu) {
          case 1:
            moh.viewRecipient(users);
            break;
          case 2:
            users = moh.supplyVaccine(users);
            break;
          case 3:
            users = moh.assignRecipient(users);
            break;
          case 4:
            moh.analytics(users);
            break;
          case 5:
            wrapper.loggedIn = false;
            break;
        }
      } catch (NumberFormatException e) {
        display.wrongMenu();
      }
    }
    mainMenu(users);
  }
  
  
  /** 
   * @param user
   * @param users
   */
  public void displayVacMenu(VaccineCenter user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    VaccineCenter vac = new VaccineCenter();
    var wrapper = new Object(){
      Boolean loggedIn = true;
    };
    display.clearConsole();
    while (wrapper.loggedIn) {
      display.println("Welcome " + user.name, "success");
      display.println("Below is what you can do : ", "input");
      display.println("1. View all recipient data and vaccine status in table", "basic");
      display.println("2. Set appointment", "basic");
      display.println("3. Update recipient status based on doses received", "basic");
      display.println("4. Statistic", "basic");
      display.println("5. Logout", "basic");
      display.print("Enter you option : ", "input");
      
      try {
        int menu = Integer.parseInt(sc.nextLine()); 
        switch (menu) {
          case 1:
            vac.viewRecipient(user, users);
            break;
          case 2:
            users = vac.setAppointment(user, users);
            break;
          case 3:
            vac.hallSimulator(user, users);
            break;
          case 4:
            vac.analytics(user, users);
            break;
          case 5:
            wrapper.loggedIn = false;
            break;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    mainMenu(users);
  }
  
  
  /** 
   * @param users
   */
  public void mainMenu(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    Boolean isWrong = true;
    Recipient recipient = new Recipient();
    while(isWrong){
      try {
        display.clearConsole();
        display.println("Welcome to MySejahtera", "success");
        display.println("Please select your menu below :", "input");
        display.println("1. Login to system", "basic");
        display.println("3. Exit system", "basic");
        display.print("Enter the number : ", "input");
        int menu = Integer.parseInt(sc.nextLine());
        if (menu == 1){
          loginLoop(users);
        }else if (menu == 3) {
          display.println("Exiting program...", "error");
          isWrong = false;
          writeFile(users);
          System.exit(0);
          break;
        }else{
          display.wrongMenu();
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
  }

  
  /** 
   * @param users
   */
  public void writeFile(User users){
    String filePath = "users.csv";
    try {
      FileWriter outputfile = new FileWriter(filePath);
      CSVWriter writer = new CSVWriter(outputfile);
      
      List<String[]> data = new ArrayList<String[]>();
      data.add(new String[] { 
        "name", "phone", "status/limit_dose", 
        "first_dose/total_dose", "second_dose/center_capacity",
        "email", "password", "type"
      });
      users.mohList.forEach(user->{
        data.add(new String[] { 
          user.name, user.phone,
          "", "", "",
          user.email, user.password, "moh"
        });
      });
      users.vacList.forEach(user->{
        data.add(new String[] { 
          user.name, user.phone,
          Integer.toString(user.limit_dose), Integer.toString(user.total_dose), Integer.toString(user.center_capacity),
          user.email, user.password, "vac"
        });
      });
      users.recipientsList.forEach(user->{
        String firstDoseString = "";
        String secondDoseString = "";
        if (user.first_dose.get("isEmpty").equals("false")){
          firstDoseString = user.first_dose.get("center") + "@" + user.first_dose.get("date") + "@" + user.first_dose.get("batch") + "@" + user.first_dose.get("date_complete");
        }
        if (user.second_dose.get("isEmpty").equals("false")){
          secondDoseString = user.second_dose.get("center") + "@" + user.second_dose.get("date") + "@" + user.second_dose.get("batch")+ "@" + user.second_dose.get("date_complete") ;
        }
        data.add(new String[] { 
          user.name, user.phone,
          user.status,
          firstDoseString, secondDoseString,
          user.email, user.password, "recipient",
          user.age
        });
      });
      writer.writeAll(data);

      // closing writer connection
      writer.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}
