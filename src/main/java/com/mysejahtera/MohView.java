package com.mysejahtera;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.*;
import java.util.*;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MohView {
  MainController controller;
  MinHealthController mohController;
  MinHealth current_user;
  int current_userInd;
  public MohView(MainController controller, int index){
    this.controller = controller;
    this.current_userInd = index;
    this.mohController = new MinHealthController();
  }
  
  public void mainMenu(){
    User users = this.controller.getUsers();
    current_user = users.mohList.get(current_userInd);
    GridPane view = new GridPane();
    Label title = new Label("      Hello, " + current_user.name);
    view.addRow(0, title);
    Button viewAll = new Button("View All Recipient");
    Button supply = new Button("Edit Distribution of Vaccine Center");
    Button assign = new Button("Assign Recipient to Vaccine Center");
    Button statistic = new Button("View Statistic");
    Button logout = new Button("Logout");
    
    view.addColumn(0, viewAll);
    view.addColumn(0, supply);
    view.addColumn(0, assign);
    view.addColumn(0, statistic);
    view.addColumn(0, logout);
    
    viewAll.setMinWidth(210);
    supply.setMinWidth(210);
    assign.setMinWidth(210);
    statistic.setMinWidth(210);
    logout.setMinWidth(210);
    title.setAlignment(Pos.CENTER);
    title.setFont(new Font("Arial", 16));
    view.setAlignment(Pos.CENTER);
    view.setVgap(3);

    Scene scene = new Scene(view, 400, 400);
    Stage stage = new Stage();
    stage.setTitle("Ministry of Health Menu");
    stage.setScene(scene);
    stage.show();

    // event listener for all button
    viewAll.setOnAction(e->{
      viewAllRec();  
    });
    supply.setOnAction(e->{
      increaseSupply();  
    });
    assign.setOnAction(e->{
      setRecipient();
    });
    logout.setOnAction(e->{
      stage.close();
    });
    statistic.setOnAction(e->{
      statistic();
    });
  }

  public void statistic(){
    GridPane view = new GridPane();
    view.setAlignment(Pos.CENTER);
    view.setVgap(3);
    // buttons row
    Button totalVac = new Button("Total Vaccination");
    Button byDay = new Button("Total By Day");
    totalVac.setMinWidth(130);
    byDay.setMinWidth(130);
    GridPane buttons = new GridPane();
    buttons.addRow(0, totalVac, byDay);
    buttons.setAlignment(Pos.CENTER);
    buttons.setHgap(5);
    view.addRow(0, buttons);
    // end buttons row
    User users = this.controller.getUsers();
    // total vac data/table
    var totalVacData = new Object(){
      String pendingTotal,firstAppTotal,firstCompTotal,secondAppTotal,secondCompTotal;
    };
    totalVacData.pendingTotal = this.mohController.totalStatus(users, "Pending");
    totalVacData.firstAppTotal = this.mohController.totalStatus(users, "1st Dose Appointment");
    totalVacData.firstCompTotal = this.mohController.totalStatus(users, "1st Dose Completed");
    totalVacData.secondAppTotal = this.mohController.totalStatus(users, "2nd Dose Appointment");
    totalVacData.secondCompTotal = this.mohController.totalStatus(users, "2nd Dose Completed");

    GridPane totalVacTable = new GridPane();
    Label test = new Label(totalVacData.pendingTotal);
    test.setAlignment(Pos.CENTER);
    totalVacTable.addRow(0, new Label("Pending - "), test);
    totalVacTable.addRow(1, new Label("1st Dose Appointment - "), new Label(totalVacData.firstAppTotal));
    totalVacTable.addRow(2, new Label("1st Dose Completed - "), new Label(totalVacData.firstCompTotal));
    totalVacTable.addRow(3, new Label("2nd Dose Appointment - "), new Label(totalVacData.secondAppTotal));
    totalVacTable.addRow(4, new Label("2nd Dose Completed - "), new Label(totalVacData.secondCompTotal));
    view.addRow(1, totalVacTable);
    totalVacTable.setVgap(4);
    totalVacTable.setAlignment(Pos.CENTER);
    // end total vac data/table

    // total by day data/table
    var byDayData = new Object(){
      String firstAppTotal, firstCompTotal, secondAppTotal, secondCompTotal;
    };
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    LocalDateTime now = LocalDateTime.now();
    byDayData.firstAppTotal = this.mohController.totalByDay(users, "1st Dose Appointment", dtf.format(now));
    byDayData.firstCompTotal = this.mohController.totalByDay(users, "1st Dose Completed", dtf.format(now));
    byDayData.secondAppTotal = this.mohController.totalByDay(users, "2nd Dose Appointment", dtf.format(now));
    byDayData.secondCompTotal = this.mohController.totalByDay(users, "2nd Dose Completed", dtf.format(now));

    GridPane byDayTable = new GridPane();
    byDayTable.addRow(1, new Label("1st Dose Appointment : "), new Label(byDayData.firstAppTotal));
    byDayTable.addRow(2, new Label("1st Dose Completed : "), new Label(byDayData.firstCompTotal));
    byDayTable.addRow(3, new Label("2nd Dose Appointment : "), new Label(byDayData.secondAppTotal));
    byDayTable.addRow(4, new Label("2nd Dose Completed : "), new Label(byDayData.secondCompTotal));
    Button submit = new Button("View Data");
    TextField dateInp = new TextField();
    GridPane inpGp = new GridPane();
    inpGp.setHgap(3);
    inpGp.addRow(0, new Label("Enter Date : "), dateInp, submit);
    byDayTable.setAlignment(Pos.CENTER);
    byDayTable.setVgap(4);
    // end total by day data/table
    submit.setOnAction(e->{
      byDayData.firstAppTotal = this.mohController.totalByDay(users, "1st Dose Appointment", dateInp.getText());
      byDayData.firstCompTotal = this.mohController.totalByDay(users, "1st Dose Completed", dateInp.getText());
      byDayData.secondAppTotal = this.mohController.totalByDay(users, "2nd Dose Appointment", dateInp.getText());
      byDayData.secondCompTotal = this.mohController.totalByDay(users, "2nd Dose Completed", dateInp.getText());
      
      // view.getChildren().remove(byDayTable);
      view.getChildren().removeIf( node -> GridPane.getRowIndex(node) == 1);
      byDayTable.getChildren().clear();
      byDayTable.addRow(1, new Label("1st Dose Appointment : "), new Label(byDayData.firstAppTotal));
      byDayTable.addRow(2, new Label("1st Dose Completed : "), new Label(byDayData.firstCompTotal));
      byDayTable.addRow(3, new Label("2nd Dose Appointment : "), new Label(byDayData.secondAppTotal));
      byDayTable.addRow(4, new Label("2nd Dose Completed : "), new Label(byDayData.secondCompTotal));
      view.addRow(1, byDayTable);
    });
    totalVac.setOnAction(e->{
      view.getChildren().removeIf( node -> GridPane.getRowIndex(node) == 1);
      view.getChildren().removeIf( node -> GridPane.getRowIndex(node) == 2);
      view.addRow(1, totalVacTable);
    });
    byDay.setOnAction(e->{
      view.getChildren().removeIf( node -> GridPane.getRowIndex(node) == 1);
      view.addRow(1, byDayTable);
      view.addRow(2, inpGp);
    });
    
    Scene scene = new Scene(view, 500, 500);
    Stage stage = new Stage();
    stage.setTitle("Statistic");
    stage.setScene(scene);
    stage.show();
  }

  public void setRecipient(){
    GridPane view = new GridPane();
    view.setAlignment(Pos.CENTER);
    Button firstDoseBtn =  new Button("First Dose");
    Button secondDoseBtn =  new Button("Second Dose");
    GridPane btnGp = new GridPane();
    btnGp.setAlignment(Pos.CENTER);
    btnGp.setHgap(5);
    btnGp.addRow(0, firstDoseBtn, secondDoseBtn);
    view.addRow(0, btnGp);
    Label recTitle = new Label("Recipient List");
    recTitle.setFont(new Font("Arial", 15));
    recTitle.setStyle("-fx-font-weight: bold;");
    view.addRow(1, recTitle);
    Group firstDoseGp = new Group();
    Group secondDoseGp = new Group();
    var helper = new Object(){
      int dose = 1;
      User users;
    };
    
    view.addRow(2, firstDoseGp);
    helper.users = this.controller.getUsers();

    TableView firstTable = new TableView();
    TableColumn<Recipient, String> noCol = new TableColumn<>("NO");
    TableColumn<Recipient, String> nameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> limitCol = new TableColumn<>("1st Dose Center");
    TableColumn<Recipient, String> totalCol = new TableColumn<>("1st Dose Date");
    noCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    limitCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("1", "center")));
    totalCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("1", "date")));
    firstTable.getColumns().addAll(noCol,nameCol,limitCol,totalCol);
    
    helper.users.recipientsList.forEach(rec ->{
      firstTable.getItems().add(rec);
    });
    firstDoseGp.getChildren().add(firstTable);

    firstTable.setMaxHeight(250);
    firstTable.setMinWidth(400);
    noCol.setMinWidth(20);
    nameCol.setMinWidth(180);
    totalCol.setMinWidth(100);
    limitCol.setMinWidth(100);
    noCol.setStyle("-fx-alignment: center");
    totalCol.setStyle("-fx-alignment: center");
    limitCol.setStyle("-fx-alignment: center");
    
    
    TableView secondTable = new TableView();
    TableColumn<Recipient, String> snoCol = new TableColumn<>("NO");
    TableColumn<Recipient, String> snameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> slimitCol = new TableColumn<>("2nd Dose Center");
    TableColumn<Recipient, String> stotalCol = new TableColumn<>("2nd Dose Date");
    snoCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    snameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    slimitCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("2", "center")));
    stotalCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("2", "date")));
    secondTable.getColumns().addAll(snoCol,snameCol,slimitCol,stotalCol);
    
    helper.users.recipientsList.forEach(rec ->{
      secondTable.getItems().add(rec);
    });
    secondDoseGp.getChildren().add(secondTable);
    
    secondTable.setMaxHeight(250);
    secondTable.setMinWidth(400);
    snoCol.setMinWidth(20);
    snameCol.setMinWidth(180);
    stotalCol.setMinWidth(100);
    slimitCol.setMinWidth(100);
    snoCol.setStyle("-fx-alignment: center");
    stotalCol.setStyle("-fx-alignment: center");
    slimitCol.setStyle("-fx-alignment: center");
    
    TableView vaccineTable = new TableView();
    TableColumn<VaccineCenter, String> vnoCol = new TableColumn<>("NO");
    TableColumn<VaccineCenter, String> vnameCol = new TableColumn<>("Name");
    TableColumn<VaccineCenter, String> vavailableCol = new TableColumn<>("Available Capacity");
    vnoCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    vnameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    vavailableCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().availableCapacity()));
    vaccineTable.getColumns().addAll(vnoCol,vnameCol,vavailableCol);
    
    helper.users.vacList.forEach(vac ->{
      vaccineTable.getItems().add(vac);
    });
    Group vacGp = new Group();
    vacGp.getChildren().add(vaccineTable);
    view.addRow(3, vacGp);

    vaccineTable.setMaxHeight(100);
    vaccineTable.setMinWidth(400);
    vnoCol.setMinWidth(20);
    vnameCol.setMinWidth(230);
    vavailableCol.setMinWidth(150);
    vnoCol.setStyle("-fx-alignment: center");
    vavailableCol.setStyle("-fx-alignment: center");


    TextField recInp = new TextField();
    TextField vacInp = new TextField();
    Button submit = new Button("Assign");
    GridPane form = new GridPane();
    form.addRow(0, new Label("Recipient No  "), recInp);
    form.addRow(1, new Label("Vaccine Center No  "), vacInp);
    form.addColumn(1, submit);
    submit.setMinWidth(150);
    form.setAlignment(Pos.CENTER);
    form.setVgap(3);
    view.setVgap(3);
    view.addRow(4, form);
    
    submit.setOnAction(e->{
      User userUpdate = this.mohController.setRecipient(helper.dose, recInp.getText(), Integer.parseInt(vacInp.getText()), helper.users);
      this.controller.updateUsers(userUpdate);
      helper.users = this.controller.getUsers();
      view.addRow(6, new Label("Successfully update recipient"));
    });
    secondDoseBtn.setOnAction(e->{
      view.getChildren().remove(firstDoseGp);
      helper.users = this.controller.getUsers();
      view.addRow(2, secondDoseGp);
      recInp.setText("");
      vacInp.setText("");
      helper.dose = 2;
    });
    firstDoseBtn.setOnAction(e->{
      view.getChildren().remove(secondDoseGp);
      helper.users = this.controller.getUsers();
      view.addRow(2, firstDoseGp);
      recInp.setText("");
      vacInp.setText("");
      helper.dose = 1;
    });
    
    Scene scene = new Scene(view, 500, 550);
    Stage stage = new Stage();
    stage.setTitle("Assign Recipient to Vaccine Center");
    stage.setScene(scene);
    stage.show();
  }

  public void increaseSupply(){
    GridPane view = new GridPane();
    Label title = new Label("List of Vaccine Center");
    view.addRow(0, title);
    
    User users = this.controller.getUsers();
    TableView tableCol = new TableView();
    TableColumn<VaccineCenter, String> noCol = new TableColumn<>("NO");
    TableColumn<VaccineCenter, String> nameCol = new TableColumn<>("Name");
    TableColumn<VaccineCenter, String> limitCol = new TableColumn<>("Limit Dose");
    TableColumn<VaccineCenter, String> totalCol = new TableColumn<>("Total Vaccined");
    TableColumn<VaccineCenter, String> centerCol = new TableColumn<>("Center Capacity");
    noCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    limitCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().limit_dose)));
    totalCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().total_dose)));
    centerCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().center_capacity)));
    tableCol.getColumns().addAll(noCol,nameCol,limitCol,totalCol,centerCol);
    
    users.vacList.forEach(vac ->{
      tableCol.getItems().add(vac);
    });
    view.addRow(1, tableCol);
    TextField centerInp = new TextField();
    TextField supplyInp = new TextField();
    GridPane form = new GridPane();
    form.addRow(0, new Label("Center Number (ID) "),centerInp);
    form.addRow(1, new Label("Amount to Increase  "),supplyInp);
    Button submit = new Button("Increase");
    form.addColumn(1, submit);
    view.addRow(2, form);
    view.addRow(3, new Label("Hint : This will increase the center capacity of the Vaccine Center!"));
    
    submit.setMinWidth(150);
    form.setVgap(3);
    view.setVgap(3);
    title.setFont(new Font("Arial", 14));
    title.setStyle("-fx-font-weight: bold;");
    view.setAlignment(Pos.CENTER);
    tableCol.setMaxHeight(100);
    noCol.setStyle("-fx-alignment: center");
    nameCol.setStyle("-fx-alignment: center");
    limitCol.setStyle("-fx-alignment: center");
    totalCol.setStyle("-fx-alignment: center");
    centerCol.setStyle("-fx-alignment: center");

    Scene scene = new Scene(view, 500, 500);
    Stage stage = new Stage();
    stage.setTitle("Edit Distribution of Vaccine Center");
    stage.setScene(scene);
    stage.show();
    submit.setOnAction(e->{
      // sent value to controller
      User usersUpdate = this.mohController.updateSupply(centerInp.getText(), supplyInp.getText(), users);
      this.controller.updateUsers(usersUpdate);
      view.addRow(6, new Label("Amount updated. Please reopen window"));
    });
  }

  public void viewAllRec(){
    GridPane view = new GridPane();
    Label title = new Label("List of Recipient");    
    title.setFont(new Font("Arial", 14));
    title.setStyle("-fx-font-weight: bold;");
    view.addRow(0, title);
    view.setAlignment(Pos.CENTER);
    User users = this.controller.getUsers();

    TableView firstTable = new TableView();
    TableColumn<Recipient, String> noCol = new TableColumn<>("NO");
    TableColumn<Recipient, String> nameCol = new TableColumn<>("Name");
    TableColumn<Recipient, String> limitCol = new TableColumn<>("1st Dose Center");
    TableColumn<Recipient, String> totalCol = new TableColumn<>("2nd Dose Center");
    noCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(Integer.toString(data.getValue().id)));
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    limitCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("1", "center")));
    totalCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getDose("2", "center")));
    firstTable.getColumns().addAll(noCol,nameCol,limitCol,totalCol);
    
    users.recipientsList.forEach(rec ->{
      firstTable.getItems().add(rec);
    });

    firstTable.setMaxHeight(400);
    firstTable.setMinWidth(400);
    noCol.setMinWidth(20);
    nameCol.setMinWidth(180);
    totalCol.setMinWidth(100);
    limitCol.setMinWidth(100);
    noCol.setStyle("-fx-alignment: center");
    totalCol.setStyle("-fx-alignment: center");
    limitCol.setStyle("-fx-alignment: center");
    view.addRow(1, firstTable);
    view.setVgap(4);
    Scene scene = new Scene(view, 500, 500);
    Stage stage = new Stage();
    stage.setTitle("List Recipient");
    stage.setScene(scene);
    stage.show();

  }
}

