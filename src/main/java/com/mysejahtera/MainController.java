package com.mysejahtera;

import java.io.IOException;
import java.util.*;

import com.opencsv.exceptions.CsvException;
public class MainController {
  User users;
  
  public MainController(User users){
    this.users = users;
  }

  
  /** 
   * @param email
   * @param password
   * @return Map<String, String>
   */
  public Map<String, String> login(String email, String password){
    var helper = new Object() {
      Boolean IsVerified = false;
      Boolean userFound = false;
      Boolean errorExist = false;
      String userType = "";
      Map<String, String> response = new HashMap<>();
      int index = 0;
    };
    
    users.recipientsList.forEach(user ->{
      Map<String, String> temp = user.getCredential();
      if (temp.get("email").equals(email)){
        helper.userFound = true;
        if (temp.get("password").equals(password)){
          helper.IsVerified = true;
          helper.response.put("type", "recipient");
          helper.response.put("userIndex", Integer.toString(helper.index) );
        }else{
          helper.errorExist = true;
        }
      }else{
        helper.errorExist = true;
      }
      helper.index++;
    });
      
    if (!helper.userFound){
      helper.index = 0;
      users.vacList.forEach(user ->{
        Map<String, String> temp = user.getCredential();
        if (temp.get("email").equals(email)){
          helper.userFound = true;
          if (temp.get("password").equals(password)){
            helper.IsVerified = true;
            helper.response.put("type", "vac");
            helper.response.put("userIndex", Integer.toString(helper.index) );
          }else{
            helper.errorExist = true;
          }
        }else{
          helper.errorExist = true;
        }
        helper.index++;
        return;
      });
    }
      
    if (!helper.userFound){
      helper.index = 0;
      users.mohList.forEach(user ->{
        Map<String, String> temp = user.getCredential();
        if (temp.get("email").equals(email)){
          helper.userFound = true;
          if (temp.get("password").equals(password)){
            helper.IsVerified = true;
            helper.response.put("type", "moh");
            helper.response.put("userIndex", Integer.toString(helper.index) );
          }else{
            helper.errorExist = true;
          }
        }else{
          helper.errorExist = true;
        }
        helper.index++;
        return;
      });
    }
    
    
    if (helper.errorExist && helper.IsVerified == false){
      helper.response.put("authenticated", "false");
      return helper.response;
    }else{
      helper.response.put("authenticated", "true");
      return helper.response;
    }
  }

  
  /** 
   * @param data
   * @return Boolean
   */
  public Boolean registerUser(Map<String, String> data, int id){
    String[] recipientData = {
      data.get("name"), data.get("phone"), "Pending", "", "", 
      data.get("username"), data.get("password"), "recipient", data.get("age")
    };
    Recipient tempRecipient = new Recipient(recipientData, id);
    users.recipientsList.add(tempRecipient);
    return true;
  }

  
  /** 
   * @throws IOException
   * @throws CsvException
   */
  public void exitSystem() throws IOException, CsvException{
    MainInit main = new MainInit();
    main.writeFile(users);
  }

  
  /** 
   * @return User
   */
  public User getUsers(){
    return this.users;
  }

  
  /** 
   * @param users
   */
  public void updateUsers(User users){
    this.users = users;
  }
  
  
  /** 
   * @param users
   * @return User
   */
  public User testupdateUsers(User users){
    this.users = users;
    return this.users;
  }

  
  /** 
   * @param user
   * @param index
   */
  public void redirectController(String user, String index){
    if (user.equals("recipient")){
      // redirect to recipient controller
    }else if(user.equals("vac")){
      // redirect to vac controller
    }else if(user.equals("moh")){
      // redirect to moh controller
      
    }
  }
}
