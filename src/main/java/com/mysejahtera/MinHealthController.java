package com.mysejahtera;

public class MinHealthController {
  User users;
  
  public MinHealthController(){}
  public MinHealthController(User users){
    this.users = users;
  }

  
  /** 
   * @param users
   * @param status
   * @return String
   */
  public String totalStatus(User users, String status){
    var helper = new Object(){
      int total;
    };
    users.recipientsList.forEach(e->{
      if (e.status.equals(status)){
        helper.total++;
      }
    });
    return Integer.toString(helper.total);
  }
  
  
  /** 
   * @param users
   * @param status
   * @param date
   * @return String
   */
  public String totalByDay(User users, String status, String date){
    var helper = new Object(){
      int total;
    };
    users.recipientsList.forEach(e->{
      if (e.status.equals(status)){
        if (e.status.substring(e.status.length() - 1).equals("t") && e.status.charAt(0) == '1'){
          if (e.first_dose.get("date").equals(date)){
            helper.total++;
          }
        }
        if (e.status.substring(e.status.length() - 1).equals("t") && e.status.charAt(0) == '2'){
          if (e.second_dose.get("date").equals(date)){
            helper.total++;
          }
        }
        if (e.status.substring(e.status.length()-1).equals("d")){
          if (e.status.charAt(0) == '1'){
            if (e.first_dose.get("date_complete").equals(date)){
              helper.total++;
            }
          }else{
            if (e.second_dose.get("date_complete").equals(date)){
              helper.total++;
            }
          }
        }
      }
    });
    return Integer.toString(helper.total);
  }

  
  /** 
   * @param centerInd
   * @param amount
   * @param users
   * @return User
   */
  public User updateSupply(String centerInd, String amount, User users){
    VaccineCenter vac = users.vacList.get(Integer.parseInt(centerInd));
    vac.setCenterCapacity(Integer.parseInt(amount));
    users.vacList.set(Integer.parseInt(centerInd), vac);
    return users;
  }

  
  /** 
   * @param dose
   * @param recInd
   * @param vacInd
   * @param users
   * @return User
   */
  public User setRecipient(int dose, String recInd, int vacInd, User users){
    int recIn = Integer.parseInt(recInd);
    Recipient recipient = users.recipientsList.get(recIn);
    VaccineCenter vac = users.vacList.get(vacInd);
    if (dose == 1){
      recipient.first_dose.put("center", vac.name);
      recipient.first_dose.put("date", "Not Assign");
      recipient.first_dose.put("batch", "0");
      recipient.first_dose.put("date_complete", "0");
      recipient.first_dose.put("isEmpty", "false");
    }else{
      recipient.second_dose.put("center", vac.name);
      recipient.second_dose.put("date", "Not Assign");
      recipient.second_dose.put("batch", "0");
      recipient.second_dose.put("date_complete", "0");
      recipient.second_dose.put("isEmpty", "false");
    }
    users.recipientsList.set(recIn, recipient);
    return users;
  }
}
