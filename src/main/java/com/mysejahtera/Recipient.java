package com.mysejahtera;

import java.util.*;


/**
 * Recipient class
 * Hold information about recipient and function
 */
public class Recipient {
  int id;
  String name,phone,status,email,password,age;
  Map<String, String> first_dose,second_dose;

  public Recipient(){};

  public Recipient (String[] data, int id){
    this.id = id;
    this.name = data[0];
    this.phone = data[1];
    this.status = data[2];
    setDoseMap(data[3], "first");
    setDoseMap(data[4], "second");
    this.email = data[5];
    this.password = data[6];
    this.age = data[8];
  }
  
  /**
   * Generate hashmap of doses receive from csv file
   * @param doseData string of dose detail with '@' as the divider
   * @param dose string of dose type
   */
  public void setDoseMap(String doseData, String dose){
    Map<String,String> map = new HashMap<String,String>();
    // System.out.println(doseData);
    if (!doseData.equals("")){
      String[] dataArr = doseData.split("@",4);
      map.put("center", dataArr[0]);
      map.put("date", dataArr[1]);
      map.put("batch", dataArr[2]);
      map.put("date_complete", dataArr[3]);
      map.put("isEmpty", "false");
    }else{
      map.put("isEmpty", "true");
    }
    if (dose == "first") {
      this.first_dose = map;
    } else {
      this.second_dose = map;
    }
  }

  
  /** 
   * @return String
   */
  // getters
  public String getName(){
    return this.name;
  }
  
  /** 
   * @param dose
   * @param key
   * @return String
   */
  public String getDose(String dose, String key){
    if (dose.equals("1")){
      if (key.equals("center")){
        String center = this.first_dose.get("isEmpty").equals("false") ? this.first_dose.get("center") : "Not assign";
        return center;
      }else if(key.equals("date")){
        String date = this.first_dose.get("isEmpty").equals("false") ? this.first_dose.get("date") : "Not assign";
        return date;
      }else if(key.equals("batch")){
        String batch = this.first_dose.get("isEmpty").equals("false") ? this.first_dose.get("batch") : "Not assign";
        return batch;
      }else if(key.equals("date_complete")){
        String date_complete = this.first_dose.get("isEmpty").equals("false") ? this.first_dose.get("date_complete") : "Not assign";
        return date_complete;
      }
    }else{
      if (key.equals("center")){
        String center = this.second_dose.get("isEmpty").equals("false") ? this.second_dose.get("center") : "Not assign";
        return center;
      }else if(key.equals("date")){
        String date = this.second_dose.get("isEmpty").equals("false") ? this.second_dose.get("date") : "Not assign";
        return date;
      }else if(key.equals("batch")){
        String batch = this.second_dose.get("isEmpty").equals("false") ? this.second_dose.get("batch") : "Not assign";
        return batch;
      }else if(key.equals("date_complete")){
        String date_complete = this.second_dose.get("isEmpty").equals("false") ? this.second_dose.get("date_complete") : "Not assign";
        return date_complete;
      }
    }
    return "Fuck";
  }
  
  /** 
   * @return String
   */
  public String getPhone(){
    return this.phone;
  }
  
  /** 
   * @return String
   */
  public String getStatus(){
    return this.status;
  }
  
  /** 
   * @return Map<String, String>
   */
  public Map<String, String> getFirstDose(){
    return this.first_dose;
  }
  
  /** 
   * @return Map<String, String>
   */
  public Map<String, String> getSecondDose(){
    return this.second_dose;
  }

  // public void setBatch

  /**
   * used for logi
   * @return hashmap of credential with key of email and password
   */
  public Map<String, String> getCredential(){
    Map<String, String> map= new HashMap<String, String>();
    map.put("email", this.email);
    map.put("password", this.password);
    return map;
  }

  /**
   * Update status to new status
   * @param status
   */
  public void updateStatus(String status){
    this.status = status;
  }

}
