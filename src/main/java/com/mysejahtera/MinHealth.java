package com.mysejahtera;

import java.util.*;
import java.util.Map.Entry;

public class MinHealth {
  String name, phone, email, password;
  public MinHealth(){};
  
  public MinHealth(String[] data){
    this.name = data[0];
    this.phone = data[1];
    this.email = data[5];
    this.password = data[6];
  }
  
  
  /** 
   * @param users
   */
  public void viewRecipient(User users){
    TerminalColor display = new TerminalColor();
    display.clearConsole();
    var wrapper = new Object(){
      int count = 1;
    };
    display.println("List recipient registered in the system", "success");
    String tableFormat = "| %-3s | %-40s | %-15s | %-15s |%n";
    System.out.format("+-----+------------------------------------------+-----------------+-----------------+%n");
    System.out.format("+ No. + Name                                     + 1st Dose Center + 2nd Dose Center +%n");
    System.out.format("+-----+------------------------------------------+-----------------+-----------------+%n");
    users.recipientsList.forEach(user ->{
      String first_center = user.first_dose.get("isEmpty").equals("false") ? user.first_dose.get("center") : "Not assign";
      String second_center = user.second_dose.get("isEmpty").equals("false") ? user.second_dose.get("center") : "Not assign";
      System.out.format(tableFormat, wrapper.count, user.name, first_center, second_center);
      wrapper.count++;
    });
    System.out.format("+-----+------------------------------------------+-----------------+-----------------+%n");
    display.enterKey();
  }

  /**
   * increase vaccine limit to vaccine center and return users(contain updated data) to main menu
   * @param users
   * @return users
   */
  public User supplyVaccine(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    System.out.println(users.vacList.size());
    display.println("List of Vaccine Center", "warning");
    var helper = new Object(){
      int count = 1;
      Boolean isWrong = true;
      int name = 0;
      Boolean isWrongInp = true;
      int supply = 0;
    };
    String leftAlignFormat = "| %-3s | %-10s | %-10s | %-14s | %-15s |%n";
    System.out.format("+-----+------------+------------+----------------+-----------------+%n");
    System.out.format("+ No. + Name       + Limit Dose + Total Vaccined + Center Capacity +%n");
    System.out.format("+-----+------------+------------+----------------+-----------------+%n");
    users.vacList.forEach(user ->{
      System.out.format(leftAlignFormat, helper.count, user.name, user.limit_dose, user.total_dose, user.center_capacity);
      // display.println(helper.count + ". " + user.name + " Total Supply : " + user.limit_dose + " Total vaccined : " + user.total_dose, "basic");
      helper.count++;
    });
    System.out.format("+-----+------------+------------+----------------+-----------------+%n");
    while(helper.isWrong){
      try {
        display.print("Enter the vaccine center to increase the center capacity [ex: 1]: ", "input");
        helper.name = Integer.parseInt(sc.nextLine());
        if (helper.name > users.vacList.size()){
          display.wrongMenu();
        }else{
          helper.isWrong = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    VaccineCenter selectedVac = users.vacList.get(helper.name - 1);
    display.println(selectedVac.name + " has been selected with center capacity of " + selectedVac.center_capacity, "basic");
    while (helper.isWrongInp){
      try {
        display.print("Enter new increment for center capacity [ex: 10] : ", "input");
        helper.supply = Integer.parseInt(sc.nextLine());
        helper.isWrongInp = false;
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    display.println("Computing value....", "warning");
    helper.supply = selectedVac.setCenterCapacity(helper.supply);
    display.println("Current total supply is " + helper.supply, "warning");
    // sending update data to main object which is users
    users.vacList.set(helper.name - 1 , selectedVac);
    display.enterKey();
    ;
    return users;
  }

  /**
   * assign recipient to vaccine center and return users(contain updated data) to main menu
   * @param users
   * @return users
   */
  public User assignRecipient(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var selectedObj = new Object(){
      Boolean isWrongRec = true;
      Boolean isWrongVac = true;
      Recipient recipient = new Recipient();
      VaccineCenter vac = new VaccineCenter();
      int indSel = 0;
      int indVac = 0;
      int countVac = 1;
      int count = 1;
      Boolean doseWrong = true;
      int dose = 0;
      Boolean continueLimit = false;
    };
    display.println("Set appointment for : ", "input");
    display.println("1. First Dose", "basic");
    display.println("2. Second Dose", "basic");
    while(selectedObj.doseWrong){
      try {
        display.print("Select which dose you want to set appointment : ", "input");
        selectedObj.dose = Integer.parseInt(sc.nextLine());
        if (selectedObj.dose > 2 || selectedObj.dose == 0){
          display.wrongMenu();          
        }else{
          selectedObj.doseWrong = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }

    display.println("List of recipient", "warning");
    String leftAlignFormat = "| %-3s | %-40s | %-15s | %-13s |%n";
    if (selectedObj.dose == 1){
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      System.out.format("+ No. + Name                                     + 1st Dose Center + 1st Dose Date +%n");
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      users.recipientsList.forEach(user->{
        String center = user.first_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.first_dose.get("center");
        String date = user.first_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.first_dose.get("date");
        System.out.format(leftAlignFormat, selectedObj.count, user.name, center, date );
        selectedObj.count++;
      });
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
    }else{
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      System.out.format("+ No. + Name                                     + 2nd Dose Center + 2nd Dose Date +%n");
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
      users.recipientsList.forEach(user->{
        String center = user.second_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.first_dose.get("center");
        String date = user.second_dose.get("isEmpty").equals("true") ? "Not Assigned" : user.second_dose.get("date");
        System.out.format(leftAlignFormat, selectedObj.count, user.name, center, date );
        selectedObj.count++;
      });
      System.out.format("+-----+------------------------------------------+-----------------+---------------+%n");
    }
    
    while(selectedObj.isWrongRec){
      try {
        display.print("Enter the recipient number [ex: 1] : ", "input");
        selectedObj.indSel = Integer.parseInt(sc.nextLine());
        int recipientSize = users.recipientsList.size();
        if (selectedObj.indSel > recipientSize){
          display.wrongMenu();
        }else{
          selectedObj.recipient = users.recipientsList.get(selectedObj.indSel - 1);
          display.println("You have select " + selectedObj.recipient.name, "basic");
          selectedObj.isWrongRec = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    
    String leftAlignFormat2 = "| %-3s | %-10s | %-18s |%n";
    System.out.format("+-----+------------+--------------------+%n");
    System.out.format("+ No. + Name       + Remaining Capacity +%n");
    System.out.format("+-----+------------+--------------------+%n");
    users.vacList.forEach(vac->{
      System.out.format(leftAlignFormat2, selectedObj.countVac, vac.name, (vac.center_capacity - vac.total_dose));
      selectedObj.countVac++;
    });
    System.out.format("+-----+------------+--------------------+%n");

    while(selectedObj.isWrongVac){
      try {
        display.print("Enter the Vaccine Center number [ex: 1] : ", "input");
        selectedObj.indVac = Integer.parseInt(sc.nextLine());
        if (selectedObj.indVac > selectedObj.countVac - 1){
          display.wrongMenu();
        }else{
          selectedObj.vac = users.vacList.get(selectedObj.indVac - 1);
          int limit = selectedObj.vac.center_capacity - selectedObj.vac.total_dose;
          if (limit < 0){
            display.println("Capacity is exceeded for the vaccine center", "error");
            display.print("Do you want to continue [Y/N]: ", "error");
            String choice = sc.nextLine();
            if (choice.equals("Y")){
              selectedObj.continueLimit = true;
            }else{
              selectedObj.continueLimit = false;
            }
          }else{
            selectedObj.continueLimit = true;
          }

          if (selectedObj.continueLimit){
            display.println("You have select " + selectedObj.vac.name, "basic");
            display.println("Assign " + selectedObj.recipient.name + " to " + selectedObj.vac.name, "basic");
            // assign recipient to vaccine center
            if (selectedObj.dose == 1){
              selectedObj.recipient.first_dose.put("center", selectedObj.vac.name);
              selectedObj.recipient.first_dose.put("date", "Not Assign");
              selectedObj.recipient.first_dose.put("batch", "0");
              selectedObj.recipient.first_dose.put("date_complete", "0");
              selectedObj.recipient.first_dose.put("isEmpty", "false");
            }else{
              selectedObj.recipient.second_dose.put("center", selectedObj.vac.name);
              selectedObj.recipient.second_dose.put("date", "Not Assign");
              selectedObj.recipient.second_dose.put("batch", "0");
              selectedObj.recipient.second_dose.put("date_complete", "0");
              selectedObj.recipient.second_dose.put("isEmpty", "false");
            }
            // update recipient data in array list
            users.recipientsList.set(selectedObj.indSel - 1 , selectedObj.recipient);
            selectedObj.isWrongVac = false;
          }
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    
    // update capacity value on vaccine center
    selectedObj.vac.updateTotalDose();
    users.vacList.set(selectedObj.indVac - 1, selectedObj.vac);
    display.enterKey();
    return users;
  }

  
  /** 
   * @param users
   */
  public void analytics(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var wrapper = new Object(){
      int count = 1;
      int recipientCounter = 0;
    };
    String leftAlignFormat = "| %-3s | %-10s | %-4d | %-4d | %-4d | %-4d |%n";
    System.out.format(leftAlignFormat, "No.","Name", "Limit Dose", "Total Vaccined", "Center Capacity", "Total Recipient");
    users.vacList.forEach(vac -> {
      wrapper.recipientCounter = 0;
      users.recipientsList.forEach(recipient->{
        if (recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(vac.name)){
            wrapper.recipientCounter++;
          }else if (recipient.second_dose.get("isEmpty").equals("false")){
            if (recipient.first_dose.get("center").equals(vac.name)){
              wrapper.recipientCounter++;
            }
          }
        }
        wrapper.recipientCounter++;
      });
      System.out.format(leftAlignFormat, wrapper.count, vac.name, vac.limit_dose, vac.total_dose, vac.center_capacity, wrapper.recipientCounter);
      wrapper.count++;
    });
    display.enterKey();;
  }

  
  /** 
   * @param users
   */
  public void analyticsMenu(User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
  }

  
  /** 
   * @param users
   */
  public void analyticsVaccineByDay(User users){
    Map<String, Integer> dates = new HashMap<String, Integer>();
    
    // store all date
    users.vacList.forEach(vac -> {
      users.recipientsList.forEach(recipient->{
        if(recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(vac.name)){
            dates.put(recipient.first_dose.get("date"), 0);
          }
        }
      });
    });
    // count date 
    users.vacList.forEach(vac -> {
      users.recipientsList.forEach(recipient->{
        if(recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(vac.name)){
          }
        }
      });
    });
    
  }
  
  
  /** 
   * @return Map<String, String>
   */
  public Map<String, String> getCredential(){
    Map<String, String> map= new HashMap<String, String>();
    map.put("email", this.email);
    map.put("password", this.password);
    return map;
  } 
}