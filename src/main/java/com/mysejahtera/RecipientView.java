package com.mysejahtera;

import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.*;

import java.util.*;

public class RecipientView {
  MainController controller;
  Recipient current_user;
  int current_userInd;
  public RecipientView(MainController controller, int index){
    this.controller = controller;
    this.current_userInd = index;
  }

  public void mainMenu(){
    GridPane view = new GridPane();
    User users = this.controller.getUsers();
    this.current_user = users.recipientsList.get(this.current_userInd);
    Label title = new Label("Welcome " + this.current_user.name);
    title.setFont(new Font("Arial", 12));
    view.addRow(0, title);
    GridPane doseTable = new GridPane();
    String first_appointment, first_batch, first_complete;
    if (this.current_user.first_dose.get("isEmpty").equals("false")){
      first_appointment = this.current_user.first_dose.get("date") + " at " + this.current_user.first_dose.get("center");
      first_complete = this.current_user.first_dose.get("date_complete") + " at " + this.current_user.first_dose.get("center");
      first_batch = this.current_user.first_dose.get("batch");
    }else{
      first_appointment = "Date not yet assigned";
      first_batch = "Not yet assigned";
      first_complete = "Not yet assign";
    }

    String second_appointment, second_batch, second_complete;
    if (this.current_user.second_dose.get("isEmpty").equals("false")){
      second_appointment = this.current_user.second_dose.get("date") + " at " + this.current_user.second_dose.get("center");
      second_complete = this.current_user.second_dose.get("date_complete") + " at " + this.current_user.second_dose.get("center");
      second_batch = this.current_user.second_dose.get("batch") ;
    }else{
      second_appointment = "Date not yet assigned";
      second_batch = "Not yet assigned";
      second_complete = "Not yet assign";
    }

    GridPane statusGp = new GridPane();
    statusGp.addRow(0, new Label("Status : "), new Label(this.current_user.status));
    view.addRow(1, statusGp);
    view.addRow(2, new Label("1st Dose Detail"));
    doseTable.addRow(0, new Label("Appointment Date"), new Label(first_appointment));
    doseTable.addRow(1, new Label("Completed Date"), new Label(first_complete));
    doseTable.addRow(2, new Label("Batch"), new Label(first_batch));
    view.addRow(3, doseTable);
    view.addRow(4, new Label("2nd Dose Detail"));
    doseTable = new GridPane();
    doseTable.addRow(3, new Label("Appointment Date"), new Label(second_appointment));
    doseTable.addRow(4, new Label("Completed Date"), new Label(second_complete));
    doseTable.addRow(5, new Label("Batch"), new Label(second_batch));
    view.addRow(5, doseTable);

    Scene scene = new Scene(view, 400, 400);
    Stage stage = new Stage();
    stage.setTitle("Recipient Menu");
    stage.setScene(scene);
    stage.show();
  }
}
