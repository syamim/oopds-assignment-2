package com.mysejahtera;

import javafx.application.Application;
import java.util.*;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;
/**
 * Class for holding all vaccine center data and method
 */
public class VaccineCenter {
  int id;
  String name, phone, email, password;
  int limit_dose, total_dose, center_capacity;

  public VaccineCenter(){};

  public VaccineCenter(String[] data, int id){    
    this.id = id;
    this.name = data[0];
    this.phone = data[1];
    this.limit_dose = Integer.parseInt(data[2]);
    this.total_dose = Integer.parseInt(data[3]);
    this.center_capacity = Integer.parseInt(data[4]);
    this.email = data[5];
    this.password = data[6];
  }

  /**
   * Print all recipient data based on the current logged in user
   * @param current_user 
   * @param users
   */
  public void viewRecipient(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    display.clearConsole();
    display.println("List of recipient at you vaccine center", "warning");
    display.println("-> First Dose List", "success");
    String tableFormat = "| %-3s | %-40s | %-21s |%n";
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    System.out.format("+ No. + Name                                     + First Dose Date       +%n");
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    var wrapper = new Object(){
      int count = 1;
      int countSec = 1;
    };
    
    users.recipientsList.forEach(user -> {
      if (user.first_dose.get("isEmpty").equals("false")){
        if (user.first_dose.get("center").equals(current_user.name)){
          System.out.format(tableFormat, wrapper.count, user.name, user.first_dose.get("date"));
          wrapper.count++;
        }
      }
    });
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    System.out.println("");
    
    display.println("-> Second Dose List", "success");
    String tableFormat2 = "| %-3s | %-40s | %-21s |%n";
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    System.out.format("+ No. + Name                                     + Second Dose Date      +%n");
    System.out.format("+-----+------------------------------------------+-----------------------+%n");
    users.recipientsList.forEach(user -> {
      if (user.second_dose.get("isEmpty").equals("false")){
        if (user.second_dose.get("center").equals(current_user.name)){
          System.out.format(tableFormat2, wrapper.countSec, user.name, user.second_dose.get("date"));
          wrapper.countSec++;
        }
      }
    });
    System.out.format("+-----+------------------------------------------+-----------------------+%n");

    display.enterKey();
  }

  /**
   * Search recipient based on the current user
   * @param current_user
   * @param users
   */
  public void searchRecipient(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    display.println("Search by name is initiated....", "warning");
    display.print("Enter the name you want to search : ", "input");
    String name = sc.nextLine();
    var wrapper = new Object(){
      int count = 1;
      Boolean foundUser = false;
    };
    
    String tableFormat = "| %-3s | %-40s | %-20s |%n";
    System.out.format("+-----+------------------------------------------+----------------------+%n");
    System.out.format("+ No. + Name                                     + Status               +%n");
    System.out.format("+-----+------------------------------------------+----------------------+%n");
    users.recipientsList.forEach(user->{
      if (user.name.toLowerCase().contains(name.toLowerCase())){
        if (!user.first_dose.get("isEmpty").equals("false")){
          if (user.first_dose.get("center").equals(current_user.name)){
            wrapper.foundUser = true;
            System.out.format(tableFormat, wrapper.count, user.name, user.status);
            wrapper.count++;
          }
        }
      }
    });
    System.out.format("+-----+------------------------------------------+----------------------+%n");

    if (wrapper.foundUser == false){
      display.println("No recipient found", "error");
    }
    display.enterKey();
  }

  /** Update limit dose when MOH update */
  public int setSupply(int amount){
    this.limit_dose += amount;
    return this.limit_dose;
  }
  
  /** Update limit dose when MOH update */
  public int setCenterCapacity(int amount){
    this.center_capacity += amount;
    return this.center_capacity;
  }

  /**
   * update total dose when updating user status
   */
  public void updateTotalDose(){
    this.total_dose += 1;
  }
  
  /**
   * Set appointment for recipient
   * @param current_user
   * @param users
   * @return users to receive in main menu
   */
  public User setAppointment(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var helper = new Object(){
      int dose = 0;
      Boolean doseWrong = true;
      int countRep = 1;
      Boolean wrongInd = true;
      Boolean foundUser = false;
      List<Recipient> recipients = new ArrayList<>();
      String listIndex = "";
    };
    display.println("Set appointment for : ", "input");
    display.println("1. First Dose", "basic");
    display.println("2. Second Dose", "basic");
    while(helper.doseWrong){
      try {
        display.print("Select which dose you want to set appointment : ", "input");
        helper.dose = Integer.parseInt(sc.nextLine());
        if (helper.dose > 2 || helper.dose == 0){
          display.wrongMenu();          
        }else{
          helper.doseWrong = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }

    display.println("List of recipient assigned to you without appointment yet", "warning");
    if (helper.dose == 1){
      String tableFormat = "| %-3s | %-40s | %-20s |%n";
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      System.out.format("+ No. + Name                                     + Status               +%n");
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      users.recipientsList.forEach(recipient ->{
        if (recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(current_user.name)){
            if (recipient.first_dose.get("date").equals("Not Assign")){
              helper.foundUser = true;
              System.out.format(tableFormat, helper.countRep, recipient.name, recipient.status);
              helper.recipients.add(recipient);
              helper.countRep++;
            }
          }
        }
      });
      System.out.format("+-----+------------------------------------------+----------------------+%n");

      if (helper.foundUser){
        display.print("Enter the recipient number [ex: 1] : ", "input");
        helper.listIndex = sc.nextLine();
        display.print("Enter date for the appointment of the recipient [ex: 31/12/2000] : ", "input");
        String appointmentDate = sc.nextLine();
        String[] recList = helper.listIndex.split(" ",-1);
        for (int i = 0; i < recList.length; i++) {
          System.out.println("fuck" + i);
          int indexRec = Integer.parseInt(recList[i]);
          Recipient selectedRecipient = helper.recipients.get(indexRec - 1);
          display.print("You have select ", "basic");
          display.println(selectedRecipient.name, "warning");
          selectedRecipient.first_dose.put("date", appointmentDate);
          selectedRecipient.updateStatus("1st Dose Appointment");
          display.print("Update appointment for ", "basic");
          display.print(selectedRecipient.name, "warning");
          display.print(" on ", "basic");
          display.println(appointmentDate, "warning");
          users.recipientsList.set(indexRec - 1, selectedRecipient);
        }
      }else{
        display.println("No recipient found", "error");
      }
    }else{
      String tableFormat = "| %-3s | %-10s | %-20s |%n";
      System.out.format("+-----+------------+----------------------+%n");
      System.out.format("+ No. + Name       + Status               +%n");
      System.out.format("+-----+------------+----------------------+%n");
      users.recipientsList.forEach(recipient ->{
        if (recipient.second_dose.get("isEmpty").equals("false")){
          if (recipient.second_dose.get("center").equals(current_user.name)){
            if (recipient.second_dose.get("date").equals("Not Assign")){
              helper.foundUser = true;
              System.out.format(tableFormat, helper.countRep, recipient.name, recipient.status);
              helper.recipients.add(recipient);
              helper.countRep++;
            }
          }
        }
      });
      System.out.format("+-----+------------+----------------------+%n");
      if (helper.foundUser){
        display.print("Enter the recipient number [ex: 1] : ", "input");
        helper.listIndex = sc.nextLine();
        display.print("Enter date for the appointment of the recipient [ex: 31/12/2000] : ", "input");
        String appointmentDate = sc.nextLine();
        String[] recList = helper.listIndex.split(" ",-1);
        for (int i = 0; i < recList.length; i++) {
          System.out.println("fuck" + i);
          int indexRec = Integer.parseInt(recList[i]);
          Recipient selectedRecipient = helper.recipients.get(indexRec - 1);
          display.print("You have select ", "basic");
          display.println(selectedRecipient.name, "warning");
          selectedRecipient.second_dose.put("date", appointmentDate);
          selectedRecipient.updateStatus("1st Dose Appointment");
          display.print("Update appointment for ", "basic");
          display.print(selectedRecipient.name, "warning");
          display.print(" on ", "basic");
          display.println(appointmentDate, "warning");
          users.recipientsList.set(indexRec - 1, selectedRecipient);
        }
      }else{
        display.println("No recipient found", "error");
      }
    }
    display.enterKey();
    return users;
  }
  
  
  /** 
   * @param current_user
   * @param users
   * @return User
   */
  public User updateStatus(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    display.println("Update recipient status", "warning");
    // display.println("List of recipient in you center", "warning");
    var helper = new Object(){
      int count = 1;
      int countOption = 1;
      Boolean isWrong = true;
      int indRec = 0;
      Boolean wrongOpt = true;
      int indOpt = 0;
      Boolean foundUser = false;
      int dose = 0;
      Boolean doseWrong = true;
      List<Recipient> recipients = new ArrayList<>();
    };
    
    display.println("Update status for : ", "input");
    display.println("1. First Dose", "basic");
    display.println("2. Second Dose", "basic");
    while(helper.doseWrong){
      try {
        display.print("Select which dose you want to update  : ", "input");
        helper.dose = Integer.parseInt(sc.nextLine());
        if (helper.dose > 2 || helper.dose == 0){
          display.wrongMenu();          
        }else{
          helper.doseWrong = false;
        }
      } catch (Exception e) {
        display.wrongMenu();
      }
    }
    
    if (helper.dose == 1){
      String tableFormat = "| %-3s | %-40s | %-20s |%n";
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      System.out.format("+ No. + Name                                     + Status               +%n");
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      users.recipientsList.forEach(recipient->{
        if (recipient.first_dose.get("isEmpty").equals("false")){
          if (recipient.first_dose.get("center").equals(current_user.name)){
            helper.foundUser = true;
            System.out.format(tableFormat, helper.count, recipient.name, recipient.status);
            helper.recipients.add(recipient);
            helper.count++;
          }
        }
      });
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      if (helper.foundUser){
        while(helper.isWrong){
          try {
            display.print("Enter the recipient number you want to select [ex: 1]: ", "input");
            helper.indRec = Integer.parseInt(sc.nextLine());
            if (helper.indRec > helper.count - 1){
              display.wrongMenu();
            }else{
              helper.isWrong = false;
            }
          } catch (Exception e) {
            display.wrongMenu();
          }
        }
        Recipient selectedRecipient = helper.recipients.get(helper.indRec - 1);
        display.println("You have select " + selectedRecipient.name, "basic");
        display.println("Status Options", "warning");
        String[] options = {"Pending", "1st Dose Completed"};
        for (String option : options){
          display.println(helper.countOption + ". " + option, "basic");
          helper.countOption++;
        }
        while (helper.wrongOpt){
          try {
            display.print("Enter the option to update with [ex: 1]: ", "input");
            helper.indOpt = Integer.parseInt(sc.nextLine());
            if (helper.indOpt > 2 || helper.indOpt == 0){
              display.wrongMenu();
            }else{
              helper.wrongOpt = false;
            }
          } catch (Exception e) {
            display.wrongMenu();
          }
        }
    
        selectedRecipient.updateStatus(options[helper.indOpt - 1]);
        // generate batch number and date of completed
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        Random rand = new Random();
        selectedRecipient.second_dose.put("batch", Integer.toString(rand.nextInt(1000)));
        selectedRecipient.second_dose.put("date_complete", dtf.format(now));
        // end generate batch number and date of completed
        display.println("Update status for " + selectedRecipient.name + " to " + options[helper.indOpt -1], "basic");
        users.recipientsList.set(helper.indRec - 1, selectedRecipient);
      }else{
        display.println("No recipient found", "error");;
      }
    }else{
      String tableFormat = "| %-3s | %-40s | %-20s |%n";
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      System.out.format("+ No. + Name                                     + Status               +%n");
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      users.recipientsList.forEach(recipient->{
        if (recipient.second_dose.get("isEmpty").equals("false")){
          if (recipient.second_dose.get("center").equals(current_user.name)){
            helper.foundUser = true;
            System.out.format(tableFormat, helper.count, recipient.name, recipient.status);
            helper.recipients.add(recipient);
            helper.count++;
          }
        }
      });
      System.out.format("+-----+------------------------------------------+----------------------+%n");
      if (helper.foundUser){
        while(helper.isWrong){
          try {
            display.print("Enter the recipient number you want to select [ex: 1]: ", "input");
            helper.indRec = Integer.parseInt(sc.nextLine());
            if (helper.indRec > helper.count - 1){
              display.wrongMenu();
            }else{
              helper.isWrong = false;
            }
          } catch (Exception e) {
            display.wrongMenu();
          }
        }
        Recipient selectedRecipient = helper.recipients.get(helper.indRec - 1);
        display.println("You have select " + selectedRecipient.name, "basic");
        display.println("Status Options", "warning");
        String[] options = {"Pending", "2nd Dose Completed"};
        for (String option : options){
          display.println(helper.countOption + ". " + option, "basic");
          helper.countOption++;
        }
        while (helper.wrongOpt){
          try {
            display.print("Enter the option to update with [ex: 1]: ", "input");
            helper.indOpt = Integer.parseInt(sc.nextLine());
            if (helper.indOpt > 2 || helper.indOpt == 0){
              display.wrongMenu();
            }else{
              helper.wrongOpt = false;
            }
          } catch (Exception e) {
            display.wrongMenu();
          }
        }
    
        selectedRecipient.updateStatus(options[helper.indOpt -1]);
        // generate batch number and date of completed
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        Random rand = new Random();
        String batchDate = Integer.toString(rand.nextInt(1000)) + "|" + dtf.format(now);
        selectedRecipient.second_dose.put("batch", batchDate);
        // generate batch number and date of completed
        display.println("Update status for " + selectedRecipient.name + " to " + options[helper.indOpt -1], "basic");
        users.recipientsList.set(helper.indRec - 1, selectedRecipient);
      }else{
        display.println("No recipient found", "error");;
      }
    }
    display.enterKey();
    return users;
  }

  
  /** 
   * @param current_user
   * @param users
   */
  public void hallSimulator(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    // get all recipient that is appointment on that date
    var helper = new Object(){
      List<Recipient> appointmentRec = new ArrayList<>();
      List<Recipient> vaccinatedRec = new ArrayList<>();
      List<Recipient> seniorRec = new ArrayList<>();
      List<Recipient> normalRec = new ArrayList<>();
      List<String> vaccineStack = new ArrayList<>();
    };
    users.recipientsList.forEach(recipient->{
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
      LocalDateTime now = LocalDateTime.now();
      Random rand = new Random();
      if(recipient.first_dose.get("isEmpty").equals("false")){
        if (recipient.first_dose.get("center").equals(current_user.name)){
          if (recipient.first_dose.get("date").equals(dtf.format(now))){
            helper.appointmentRec.add(recipient);
            helper.vaccineStack.add(Integer.toString(rand.nextInt(1000)));
            int age = Integer.parseInt(recipient.age);
            if (age < 60){
              helper.normalRec.add(recipient);
            }else{
              helper.seniorRec.add(recipient);
            }
          }
        }
      }
      if(recipient.second_dose.get("isEmpty").equals("false")){
        if (recipient.second_dose.get("center").equals(current_user.name)){
          if (recipient.second_dose.get("date").equals(dtf.format(now))){
            helper.appointmentRec.add(recipient);
            helper.vaccineStack.add(Integer.toString(rand.nextInt(1000)));
            int age = Integer.parseInt(recipient.age);
            if (age < 60){
              helper.normalRec.add(recipient);
            }else{
              helper.seniorRec.add(recipient);
            }
          }
        }
      }
    });
    // run interface
    // next button exist
    // while next button return true and list is not empty yet
    do{
      // print list
      display.println("Senior Queue", "success");
      String tableFormat = "| %-40s | %-9s |%n";
      if(helper.seniorRec.size() != 0){
        System.out.format("+------------------------------------------+-----------+%n");
        System.out.format("+ Name                                     +     Age   +%n");
        System.out.format("+------------------------------------------+-----------+%n");
        helper.seniorRec.forEach(recipient->{
          System.out.format(tableFormat, recipient.name, recipient.age);
        });
        System.out.format("+------------------------------------------+-----------+%n");
      }else{
        display.println("Empty", "warnin");
      }
      
      System.out.println("");
      display.println("Normal Queue", "success");
      if (helper.normalRec.size() != 0){
        System.out.format("+------------------------------------------+-----------+%n");
        System.out.format("+ Name                                     +     Age   +%n");
        System.out.format("+------------------------------------------+-----------+%n");
        helper.normalRec.forEach(recipient->{
          System.out.format(tableFormat, recipient.name, recipient.age);
        });
        System.out.format("+------------------------------------------+-----------+%n");
      }else{
        display.println("Empty", "warning");
      }
      
      System.out.println("");
      display.println("Stack of Vaccine", "success");
      if (helper.appointmentRec.size() != 0){
        String vaccineStackFormat = "| %-12s |%n";
        System.out.format("+--------------+%n");
        System.out.format("+ Batch Number +%n");
        System.out.format("+--------------+%n");
        helper.vaccineStack.forEach(batch->{
          System.out.format(vaccineStackFormat, batch);
        });
        System.out.format("+--------------+%n");
      }else{
        display.println("Empty", "warning");
      }
      
      System.out.println("");
      display.println("Vaccinated", "success");
      if (helper.vaccinatedRec.size() != 0){
        String vaccinatedFormat = "| %-12s | %-40s | %-9s |%n";
        System.out.format("+--------------+------------------------------------------+-----------+%n");
        System.out.format("+ Batch Number + Name                                     +     Age   +%n");
        System.out.format("+--------------+------------------------------------------+-----------+%n");
        var fuck = new Object(){
          int count = 1;
        };
        helper.vaccinatedRec.forEach(recipient->{
          System.out.format(vaccinatedFormat, fuck.count,recipient.name, recipient.age);
        });
        System.out.format("+--------------+------------------------------------------+-----------+%n");
      }else{
        display.println("Empty", "warning");
      }
      // end print list
      // display.print("Fuck :", "warning");
      // int shit = sc.nextInt();
      // if (shit == 0){
      //   display.println("babi", "error");
      // }
      Application.launch(App.class, "babi"); // launch next button
      // update recipient status
      if (helper.appointmentRec.size() != 0){
        helper.appointmentRec.remove(helper.appointmentRec.size()-1);
        helper.vaccineStack.remove(helper.vaccineStack.size()-1);
        // äda problem dgn logic for looping
        // if (helper.seniorRec.size() != 0){
        //   helper.vaccinatedRec.add(helper.seniorRec.get(helper.seniorRec.size()-1));
        //   helper.seniorRec.remove(helper.seniorRec.size() - 1);
        // }else{
        //   helper.vaccinatedRec.add(helper.seniorRec.get(helper.seniorRec.size()-1));
        //   helper.normalRec.remove(helper.seniorRec.size()-1);
        // }
      }
      // end update recipient status 
    }while(helper.appointmentRec.size() != 0);
  }
  
  public String availableCapacity(){
    return Integer.toString(this.center_capacity - this.total_dose);
  }
  
  /** 
   * @param current_user
   * @param users
   */
  public void analytics(VaccineCenter current_user, User users){
    TerminalColor display = new TerminalColor();
    Scanner sc = new Scanner(System.in);
    display.clearConsole();
    var helper = new Object(){
      int count = 0;
    };
    users.recipientsList.forEach(recipient->{
      if (recipient.status.equals("Pending")){
        helper.count++;
      }
    });
    display.println("Total Unvaccinated Recipient : " + current_user.total_dose, "basic");
    display.println("Total Vaccinated Recipient : " + current_user.total_dose, "basic");
    display.enterKey();
  } 


  /** 
   * @return Map<String, String>
   */
  public Map<String, String> getCredential(){
    Map<String, String> map= new HashMap<String, String>();
    map.put("email", this.email);
    map.put("password", this.password);
    return map;
  }
}