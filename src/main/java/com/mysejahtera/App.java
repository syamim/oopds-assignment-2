package com.mysejahtera;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application{
  
  
  /** 
   * @param primaryStage
   * @throws Exception
   */
  @Override
  public void start(Stage primaryStage) throws Exception{
    // Model
    MainInit main = new MainInit();
    User users = main.readCsv();
    // Controller
    MainController controller = new MainController(users);
    // View
    AppView view = new AppView(users, controller);
    
    Scene scene = new Scene(view.asParent(), 300, 300);
    primaryStage.setTitle("MySejahtera Clone");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  
  /** 
   * @param args
   */
  public static void main(String[] args){
    launch(args);
  }
}
